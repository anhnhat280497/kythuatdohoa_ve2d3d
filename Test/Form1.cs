﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
            label2.Visible = label3.Visible = true;
		}
		void NetDut(int x1, int y1, int x2, int y2, Bitmap bmp, Color a)
		{
			for (int i = y1; i < y2; i++)
			{
				if (i % 4 == 0 && i != y1 && i != y2)
				{
					for (int j = 0; j < 4; j++)
						bmp.SetPixel(x1, i + j, Color.White);
					i += 4;
				}
			}
			int x = 4, y =5;
			if ((x % 2 == 0 || x % 3 == 0) && x % 6 != 0)
				bmp.SetPixel(x, y, Color.White);
			else
				bmp.SetPixel(x, y, a);
		}
		void HoanVi(ref int a, ref int b)
		{
			int t;
			t = a;
			a = b;
			b = t;
		}
        void MidpointLineXY(int x1, int y1, int x2, int y2, Bitmap bmp, Color a) //x++ , y++ Dx>=Dy 
		{
			if (x1 > x2)
			{
				HoanVi(ref x1, ref x2);
				HoanVi(ref y1, ref y2);
			}
			int x = x1, y = y1;
            int Dy = Math.Abs(y2 - y1);
            bmp.SetPixel(x1, y1, a);
			double p;
			float A = y2 - y1, B = -(x2 - x1), C = x2 * y1 - x1 * y2;
			while (x < x2)
			{
				if(y1<y2 && Dy!=0)
                {
                    p = A * (x + 1) + B * (y + 0.5) + C;
                    if(p>=0) y++;
                }
                else if(y1>y2 && Dy!=0)
                {
                    p = A * (x + 1) + B * (y - 0.5) + C;
                    if(p<=0) y--;
                }
                x++;
                bmp.SetPixel(x, y, a);
			}
		}
        void MidpointLineYX(int x1, int y1, int x2, int y2, Bitmap bmp, Color a) //y++ , x++ Dy>Dx 
        {
            if (y1 > y2)
            {
                HoanVi(ref x1, ref x2);
                HoanVi(ref y1, ref y2);
            }
            int Dx = Math.Abs(x2 - x1);
            int x = x1, y = y1;
            bmp.SetPixel(x1, y1, a);
            double p;
            float A = y2 - y1, B = -(x2 - x1), C = x2 * y1 - x1 * y2;
            while (y < y2)
            {
                if(x1<x2 && Dx !=0)
                {
                    p = A * (x + 0.5) + B * (y + 1) + C;
                    if (p <= 0) x++;
                }
                else if(x1>x2 && Dx!=0)
                {
                    p = A * (x - 0.5) + B * (y + 1) + C;
                    if (p >= 0) x--;
                }
                y++;
                bmp.SetPixel(x, y, a);
            }
        }
        void MidpointLine(int x1, int y1, int x2, int y2, Bitmap bmp, Color a)
		{
			int Dx = Math.Abs(x2 - x1), Dy = Math.Abs(y2-y1);
            if (Dx >= Dy) MidpointLineXY(x1, y1, x2, y2, bmp, a);
            else MidpointLineYX(x1, y1, x2, y2, bmp, a);
		}

        void Ve8Diem(int x, int y, int x1, int y1, int r, Bitmap bmp, Color a)
        {
            if(y-y1>=0 && x+x1<1270) bmp.SetPixel(x + x1, y - y1, a);
            if ((y + y1 <= 730) && (x + x1 < 1270)) bmp.SetPixel(x + x1, y + y1, a);
            if((x-x1)>=0 && (y + y1 )<= 730) bmp.SetPixel(x - x1, y + y1, a);
            if(x-x1>=0 && y-y1>=0)bmp.SetPixel(x - x1, y - y1, a);

            if ((y - x1 >=0) && (x + y1 < 1270)) bmp.SetPixel(x + y1, y - x1, a);
            if (x + y1 < 1270 && y + x1 <= 730) bmp.SetPixel(x + y1, y + x1, a);
            if (x - y1 >= 0 && y + x1 <= 730) bmp.SetPixel(x - y1, y + x1, a);
            if (x - y1 >= 0 && y - x1 > 0) bmp.SetPixel(x - y1, y - x1, a);
        }

        void MidpointDuongTron(int x, int y, int r, Bitmap bmp, Color a)
        {
            int x1 = 0, y1 = r;
            double p;
            Ve8Diem(x, y, x1, y1, r, bmp, a);
            bmp.SetPixel(x, y, a);
            while (x1 <((r*Math.Sqrt(2))/2))
            {
                x1++;
                p = (x1) * (x1) + (y1-0.5) *( y1-0.5) - r * r;
                if (p >= 0) y1--;
                Ve8Diem(x, y, x1, y1, r, bmp, a);
            }
        }

        void VeHinhNguoi(int x, int y, int r, Bitmap bmp, Color a)
        {
            MidpointDuongTron(x, y-20, r, bmp, a); // đầu
            MidpointLine(x, y, x, y+50, bmp, a); //mình

            MidpointLine(x, y, x-30, y+10, bmp, a); // tay trái
            MidpointLine(x, y, x+30, y+10, bmp,a ); // tay phải

            MidpointLine(x, y+50, x+20, y+100, bmp, a); // chân phải
            MidpointLine(x-20, y+100, x, y+50, bmp, a); // chân trái

        }
        void VeHinhNguoiXoay(int x, int y, int r, Bitmap bmp, Color a)
        {
            MidpointDuongTron(x, y - 20, r, bmp, a); // đầu

            MidpointLine(x, y, x, y + 50, bmp, a); //mình

            MidpointLine(x, y, x - 30, y + 10, bmp, a); // tay trái
            MidpointLine(x, y, x + 30, y + 10, bmp, a); // tay phải

            MidpointLine(x, y + 50, x + 20, y + 100, bmp, a); // chân phải
            MidpointLine(x - 20, y + 100, x, y + 50, bmp, a); // chân trái

        }
        void VeHinhKiem(int x, int y, Bitmap bmp, Color a)
        {
            MidpointLine(x-25, y-7, x-5, y-7, bmp, a);
            MidpointLine(x-25, y-3, x-5, y-3, bmp, a);
            MidpointLine(x-25, y-7, x-25, y-3, bmp, a);
            MidpointLine(x-5, y-15, x-5, y+5, bmp, a);
            MidpointLine(x-5, y-15, x, y-15, bmp, a);
            MidpointLine(x-5, y+5, x, y+5, bmp, a);
            MidpointLine(x, y-15, x, y+5, bmp, a);

            MidpointLine(x, y-10, x+45, y-10, bmp, a);
            MidpointLine(x, y, x+45, y, bmp, a);
            MidpointLine(x+45, y, x+55, y-5, bmp, a);
            MidpointLine(x+45, y-10, x+55, y-5, bmp, a);
        }
		private void Form1_Paint_1(object sender, PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(1270, 730);

            e.Graphics.DrawLine(Pens.Black, 200,350,1000,350);
			e.Graphics.DrawLine(Pens.Black, 995, 345, 1000, 350);
			e.Graphics.DrawLine(Pens.Black, 995, 355, 1000, 350);

			e.Graphics.DrawLine(Pens.Black, 600, 1, 600, 600);
			e.Graphics.DrawLine(Pens.Black, 595, 6, 600, 1);
			e.Graphics.DrawLine(Pens.Black, 605, 6, 600, 1);

			for (int i= 0; i<790; i+=10)
				e.Graphics.DrawLine(Pens.Black, 210+i, 347, 210+i, 353);

			for (int i = 0; i < 590; i += 10)
				e.Graphics.DrawLine(Pens.Black, 597, 10+i, 603, 10+i);


            //MidpointLine(500, 200, 300, 300, bmp);
            //MidpointLine(500, 200, 950, 200, bmp);
            //MidpointLine(950, 200, 750, 300, bmp);
            //MidpointLine(300, 300, 750, 300, bmp);

            //MidpointLine(330, 300, 330, 500, bmp);
            //MidpointLine(330, 500, 300, 500, bmp);
            //MidpointLine(300, 500, 300, 300, bmp);

            //MidpointLine(720, 500, 750, 500, bmp);
            //MidpointLine(750, 500, 750, 300, bmp);
            //MidpointLine(720, 300, 720, 500, bmp);

            //MidpointLine(500, 300, 500, 400, bmp);
            //MidpointLine(500, 400, 530, 400, bmp);
            //MidpointLine(530, 400, 530, 300, bmp);

            //MidpointLine(950, 200, 950, 400, bmp);
            //MidpointLine(950, 400, 920, 400, bmp);
            //MidpointLine(920, 400, 920, 215, bmp);

            //MidpointDuongTron(1000, 400, 300, bmp);
            //MidpointDuongTron(200, 400, 2, bmp);
            
            
            VeHinhNguoi(300, 250, 20, bmp, Color.Red);// người trái

            VeHinhNguoi(800, 250, 20, bmp, Color.Blue);// người phải
            MidpointLine(50, 1, 40, 100, bmp, Color.Red);
            MidpointLine(50, 1, 50, 100, bmp, Color.Red);
            MidpointLine(50, 100, 400, 100, bmp, Color.Red);
            MidpointLine(40, 1, 50, 100, bmp, Color.Red);
            MidpointLine(50, 100, 400, 120, bmp, Color.Red);
            MidpointLine(50, 100, 400, 80, bmp, Color.Red);


            int x;

            //MidpointDuongTron(x, y - 20, r, bmp, a); // đầu


            VeHinhKiem(355, 265, bmp, Color.Red);

            for (int i = 0; i < 400; i += 10)
            {
                VeHinhKiem(355 + i, 265, bmp, Color.Red);
                e.Graphics.DrawImage(bmp, 0, 0);
                Thread.Sleep(1);
                VeHinhKiem(355 + i, 265, bmp, Color.White);
                e.Graphics.DrawImage(bmp, 0, 0);
            }
            VeHinhKiem(355 + 400, 265, bmp, Color.Red);
            e.Graphics.DrawImage(bmp, 0, 0);

            bmp.Dispose();
		}

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
