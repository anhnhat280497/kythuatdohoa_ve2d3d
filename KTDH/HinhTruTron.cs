﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KTDH
{
    class HinhTruTron
    {
        public void VeHinh(double x, double y, double z, double r, double c, PaintEventArgs e, Bitmap bmp)
        {
            DuongThang dth = new DuongThang();
            ELip el = new ELip();
            z *= 10; z /= 2; x *= 10; y *= 10; r *= 10; c *= 10;
            Point a0 = new Point((int)(x - z + 500), (int)(350 - y + z));
            Point a1 = new Point((int)(x - z + r + 500), (int)(350 - y + z));
            Point a2 = new Point((int)(x - z + r + 500), (int)(350 - y + z - c));
            Point a3 = new Point((int)(x - z - r + 500), (int)(350 - y + z));
            Point a4 = new Point((int)(x - z - r + 500), (int)(350 - y + z - c));
            Point a5 = new Point((int)(x - z + 500), (int)(350 - y + z - c));
            dth.MidpointLine(a2.X, a2.Y,a1.X, a1.Y,bmp,Color.Red);
            dth.MidpointLine(a3.X, a3.Y, a4.X, a4.Y, bmp, Color.Red);
            el.NetDut(a0.X, a0.Y, (int)r, (int)r/2, bmp, Color.Red);
            el.HinhELip(a5.X, a5.Y, (int)r, (int)r/2, bmp, Color.Red);
            e.Graphics.DrawImage(bmp, 0, 0);
        }
    }
}
