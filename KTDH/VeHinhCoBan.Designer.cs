﻿namespace KTDH
{
    partial class VeHinhCoBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.controlVe = new System.Windows.Forms.Panel();
            this.goc1CD = new System.Windows.Forms.TextBox();
            this.y2cd = new System.Windows.Forms.TextBox();
            this.y1cd = new System.Windows.Forms.TextBox();
            this.x2cd = new System.Windows.Forms.TextBox();
            this.x1cd = new System.Windows.Forms.TextBox();
            this.gocQuayCD = new System.Windows.Forms.Button();
            this.thucHienBienDoi = new System.Windows.Forms.Button();
            this.tinhTienCon2 = new System.Windows.Forms.Button();
            this.tinhTienCon1 = new System.Windows.Forms.Button();
            this.tyLeCD = new System.Windows.Forms.Button();
            this.doiXungCD = new System.Windows.Forms.Button();
            this.quayCD = new System.Windows.Forms.Button();
            this.tinhTienCD = new System.Windows.Forms.Button();
            this.hcn7 = new System.Windows.Forms.TextBox();
            this.hcn3 = new System.Windows.Forms.TextBox();
            this.hcn8 = new System.Windows.Forms.TextBox();
            this.hcn4 = new System.Windows.Forms.TextBox();
            this.hcn6 = new System.Windows.Forms.TextBox();
            this.hcn2 = new System.Windows.Forms.TextBox();
            this.hcn10 = new System.Windows.Forms.Button();
            this.hcn9 = new System.Windows.Forms.Button();
            this.hcn5 = new System.Windows.Forms.Button();
            this.hcn1 = new System.Windows.Forms.Button();
            this.hinhTru = new System.Windows.Forms.Button();
            this.hinhHCN = new System.Windows.Forms.Button();
            this.chuyenDong2 = new System.Windows.Forms.Button();
            this.chuyenDong1 = new System.Windows.Forms.Button();
            this.hinh2 = new System.Windows.Forms.Button();
            this.hinh1 = new System.Windows.Forms.Button();
            this.ve3D = new System.Windows.Forms.Button();
            this.ve2D = new System.Windows.Forms.Button();
            this.toaDoCacDiem = new System.Windows.Forms.Panel();
            this.labelh7 = new System.Windows.Forms.Label();
            this.labelh6 = new System.Windows.Forms.Label();
            this.labelh5 = new System.Windows.Forms.Label();
            this.labelh4 = new System.Windows.Forms.Label();
            this.labelh1 = new System.Windows.Forms.Label();
            this.labelh2 = new System.Windows.Forms.Label();
            this.labelh3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.d27 = new System.Windows.Forms.Label();
            this.d26 = new System.Windows.Forms.Label();
            this.d25 = new System.Windows.Forms.Label();
            this.d24 = new System.Windows.Forms.Label();
            this.d23 = new System.Windows.Forms.Label();
            this.d22 = new System.Windows.Forms.Label();
            this.d1 = new System.Windows.Forms.Label();
            this.d21 = new System.Windows.Forms.Label();
            this.d2 = new System.Windows.Forms.Label();
            this.d20 = new System.Windows.Forms.Label();
            this.d3 = new System.Windows.Forms.Label();
            this.d19 = new System.Windows.Forms.Label();
            this.d4 = new System.Windows.Forms.Label();
            this.d18 = new System.Windows.Forms.Label();
            this.d5 = new System.Windows.Forms.Label();
            this.d17 = new System.Windows.Forms.Label();
            this.d16 = new System.Windows.Forms.Label();
            this.d6 = new System.Windows.Forms.Label();
            this.d15 = new System.Windows.Forms.Label();
            this.d7 = new System.Windows.Forms.Label();
            this.d14 = new System.Windows.Forms.Label();
            this.d8 = new System.Windows.Forms.Label();
            this.d13 = new System.Windows.Forms.Label();
            this.d9 = new System.Windows.Forms.Label();
            this.d12 = new System.Windows.Forms.Label();
            this.d10 = new System.Windows.Forms.Label();
            this.d11 = new System.Windows.Forms.Label();
            this.heTrucToaDo = new System.Windows.Forms.Panel();
            this.t1 = new System.Windows.Forms.Label();
            this.t35 = new System.Windows.Forms.Label();
            this.t2 = new System.Windows.Forms.Label();
            this.t36 = new System.Windows.Forms.Label();
            this.t3 = new System.Windows.Forms.Label();
            this.t37 = new System.Windows.Forms.Label();
            this.t4 = new System.Windows.Forms.Label();
            this.t38 = new System.Windows.Forms.Label();
            this.t5 = new System.Windows.Forms.Label();
            this.t39 = new System.Windows.Forms.Label();
            this.t6 = new System.Windows.Forms.Label();
            this.t40 = new System.Windows.Forms.Label();
            this.t7 = new System.Windows.Forms.Label();
            this.t8 = new System.Windows.Forms.Label();
            this.t9 = new System.Windows.Forms.Label();
            this.t10 = new System.Windows.Forms.Label();
            this.t41 = new System.Windows.Forms.Label();
            this.t11 = new System.Windows.Forms.Label();
            this.t42 = new System.Windows.Forms.Label();
            this.t12 = new System.Windows.Forms.Label();
            this.t43 = new System.Windows.Forms.Label();
            this.t13 = new System.Windows.Forms.Label();
            this.t34 = new System.Windows.Forms.Label();
            this.t33 = new System.Windows.Forms.Label();
            this.t32 = new System.Windows.Forms.Label();
            this.t31 = new System.Windows.Forms.Label();
            this.t30 = new System.Windows.Forms.Label();
            this.t29 = new System.Windows.Forms.Label();
            this.t28 = new System.Windows.Forms.Label();
            this.t27 = new System.Windows.Forms.Label();
            this.t26 = new System.Windows.Forms.Label();
            this.t25 = new System.Windows.Forms.Label();
            this.t24 = new System.Windows.Forms.Label();
            this.t23 = new System.Windows.Forms.Label();
            this.t22 = new System.Windows.Forms.Label();
            this.t21 = new System.Windows.Forms.Label();
            this.t20 = new System.Windows.Forms.Label();
            this.t19 = new System.Windows.Forms.Label();
            this.t18 = new System.Windows.Forms.Label();
            this.t17 = new System.Windows.Forms.Label();
            this.t46 = new System.Windows.Forms.Label();
            this.t16 = new System.Windows.Forms.Label();
            this.t45 = new System.Windows.Forms.Label();
            this.t15 = new System.Windows.Forms.Label();
            this.t44 = new System.Windows.Forms.Label();
            this.t14 = new System.Windows.Forms.Label();
            this.dV = new System.Windows.Forms.Label();
            this.dU = new System.Windows.Forms.Label();
            this.dT = new System.Windows.Forms.Label();
            this.dS = new System.Windows.Forms.Label();
            this.dR = new System.Windows.Forms.Label();
            this.dQ = new System.Windows.Forms.Label();
            this.diemP = new System.Windows.Forms.Label();
            this.diemO = new System.Windows.Forms.Label();
            this.diemN = new System.Windows.Forms.Label();
            this.diemM = new System.Windows.Forms.Label();
            this.diemL = new System.Windows.Forms.Label();
            this.diemK = new System.Windows.Forms.Label();
            this.diemJ = new System.Windows.Forms.Label();
            this.diemI = new System.Windows.Forms.Label();
            this.diemH = new System.Windows.Forms.Label();
            this.diemG = new System.Windows.Forms.Label();
            this.diemF = new System.Windows.Forms.Label();
            this.diemE = new System.Windows.Forms.Label();
            this.diemD = new System.Windows.Forms.Label();
            this.diemC = new System.Windows.Forms.Label();
            this.diemB = new System.Windows.Forms.Label();
            this.da = new System.Windows.Forms.Label();
            this.dZ = new System.Windows.Forms.Label();
            this.dY = new System.Windows.Forms.Label();
            this.dX = new System.Windows.Forms.Label();
            this.dW = new System.Windows.Forms.Label();
            this.diemA = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.controlVe.SuspendLayout();
            this.toaDoCacDiem.SuspendLayout();
            this.heTrucToaDo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1024F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.heTrucToaDo, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1284, 691);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.controlVe, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.toaDoCacDiem, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(254, 685);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // controlVe
            // 
            this.controlVe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.controlVe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.controlVe.Controls.Add(this.goc1CD);
            this.controlVe.Controls.Add(this.y2cd);
            this.controlVe.Controls.Add(this.y1cd);
            this.controlVe.Controls.Add(this.x2cd);
            this.controlVe.Controls.Add(this.x1cd);
            this.controlVe.Controls.Add(this.gocQuayCD);
            this.controlVe.Controls.Add(this.thucHienBienDoi);
            this.controlVe.Controls.Add(this.tinhTienCon2);
            this.controlVe.Controls.Add(this.tinhTienCon1);
            this.controlVe.Controls.Add(this.tyLeCD);
            this.controlVe.Controls.Add(this.doiXungCD);
            this.controlVe.Controls.Add(this.quayCD);
            this.controlVe.Controls.Add(this.tinhTienCD);
            this.controlVe.Controls.Add(this.hcn7);
            this.controlVe.Controls.Add(this.hcn3);
            this.controlVe.Controls.Add(this.hcn8);
            this.controlVe.Controls.Add(this.hcn4);
            this.controlVe.Controls.Add(this.hcn6);
            this.controlVe.Controls.Add(this.hcn2);
            this.controlVe.Controls.Add(this.hcn10);
            this.controlVe.Controls.Add(this.hcn9);
            this.controlVe.Controls.Add(this.hcn5);
            this.controlVe.Controls.Add(this.hcn1);
            this.controlVe.Controls.Add(this.hinhTru);
            this.controlVe.Controls.Add(this.hinhHCN);
            this.controlVe.Controls.Add(this.chuyenDong2);
            this.controlVe.Controls.Add(this.chuyenDong1);
            this.controlVe.Controls.Add(this.hinh2);
            this.controlVe.Controls.Add(this.hinh1);
            this.controlVe.Controls.Add(this.ve3D);
            this.controlVe.Controls.Add(this.ve2D);
            this.controlVe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlVe.Location = new System.Drawing.Point(3, 3);
            this.controlVe.Name = "controlVe";
            this.controlVe.Size = new System.Drawing.Size(248, 336);
            this.controlVe.TabIndex = 0;
            // 
            // goc1CD
            // 
            this.goc1CD.Location = new System.Drawing.Point(203, 210);
            this.goc1CD.Name = "goc1CD";
            this.goc1CD.Size = new System.Drawing.Size(35, 20);
            this.goc1CD.TabIndex = 9;
            this.goc1CD.Text = "0";
            this.goc1CD.Visible = false;
            // 
            // y2cd
            // 
            this.y2cd.Location = new System.Drawing.Point(203, 264);
            this.y2cd.Name = "y2cd";
            this.y2cd.Size = new System.Drawing.Size(35, 20);
            this.y2cd.TabIndex = 9;
            this.y2cd.Text = "0";
            this.y2cd.Visible = false;
            // 
            // y1cd
            // 
            this.y1cd.Location = new System.Drawing.Point(203, 174);
            this.y1cd.Name = "y1cd";
            this.y1cd.Size = new System.Drawing.Size(35, 20);
            this.y1cd.TabIndex = 9;
            this.y1cd.Text = "0";
            this.y1cd.Visible = false;
            // 
            // x2cd
            // 
            this.x2cd.Location = new System.Drawing.Point(203, 243);
            this.x2cd.Name = "x2cd";
            this.x2cd.Size = new System.Drawing.Size(35, 20);
            this.x2cd.TabIndex = 9;
            this.x2cd.Text = "0";
            this.x2cd.Visible = false;
            // 
            // x1cd
            // 
            this.x1cd.Location = new System.Drawing.Point(203, 153);
            this.x1cd.Name = "x1cd";
            this.x1cd.Size = new System.Drawing.Size(35, 20);
            this.x1cd.TabIndex = 9;
            this.x1cd.Text = "0";
            this.x1cd.Visible = false;
            // 
            // gocQuayCD
            // 
            this.gocQuayCD.Location = new System.Drawing.Point(95, 209);
            this.gocQuayCD.Name = "gocQuayCD";
            this.gocQuayCD.Size = new System.Drawing.Size(84, 23);
            this.gocQuayCD.TabIndex = 8;
            this.gocQuayCD.Text = "Góc:";
            this.gocQuayCD.UseVisualStyleBackColor = true;
            this.gocQuayCD.Visible = false;
            // 
            // thucHienBienDoi
            // 
            this.thucHienBienDoi.BackColor = System.Drawing.Color.Red;
            this.thucHienBienDoi.Location = new System.Drawing.Point(77, 292);
            this.thucHienBienDoi.Name = "thucHienBienDoi";
            this.thucHienBienDoi.Size = new System.Drawing.Size(88, 27);
            this.thucHienBienDoi.TabIndex = 7;
            this.thucHienBienDoi.Text = "Biến đổi";
            this.thucHienBienDoi.UseVisualStyleBackColor = false;
            this.thucHienBienDoi.Visible = false;
            this.thucHienBienDoi.Click += new System.EventHandler(this.ThucHienBienDoi_Click);
            // 
            // tinhTienCon2
            // 
            this.tinhTienCon2.Location = new System.Drawing.Point(95, 250);
            this.tinhTienCon2.Name = "tinhTienCon2";
            this.tinhTienCon2.Size = new System.Drawing.Size(101, 23);
            this.tinhTienCon2.TabIndex = 6;
            this.tinhTienCon2.Text = "Khoảng cách x, y:";
            this.tinhTienCon2.UseVisualStyleBackColor = true;
            this.tinhTienCon2.Visible = false;
            // 
            // tinhTienCon1
            // 
            this.tinhTienCon1.Location = new System.Drawing.Point(95, 160);
            this.tinhTienCon1.Name = "tinhTienCon1";
            this.tinhTienCon1.Size = new System.Drawing.Size(101, 23);
            this.tinhTienCon1.TabIndex = 6;
            this.tinhTienCon1.Text = "Khoảng cách x, y:";
            this.tinhTienCon1.UseVisualStyleBackColor = true;
            this.tinhTienCon1.Visible = false;
            // 
            // tyLeCD
            // 
            this.tyLeCD.Location = new System.Drawing.Point(5, 250);
            this.tyLeCD.Name = "tyLeCD";
            this.tyLeCD.Size = new System.Drawing.Size(75, 23);
            this.tyLeCD.TabIndex = 5;
            this.tyLeCD.Text = "Tỷ lệ";
            this.tyLeCD.UseVisualStyleBackColor = true;
            this.tyLeCD.Visible = false;
            this.tyLeCD.Click += new System.EventHandler(this.TyLe_Click);
            // 
            // doiXungCD
            // 
            this.doiXungCD.Location = new System.Drawing.Point(5, 220);
            this.doiXungCD.Name = "doiXungCD";
            this.doiXungCD.Size = new System.Drawing.Size(75, 23);
            this.doiXungCD.TabIndex = 5;
            this.doiXungCD.Text = "Đối xứng";
            this.doiXungCD.UseVisualStyleBackColor = true;
            this.doiXungCD.Visible = false;
            this.doiXungCD.Click += new System.EventHandler(this.DoiXung_Click);
            // 
            // quayCD
            // 
            this.quayCD.Location = new System.Drawing.Point(5, 190);
            this.quayCD.Name = "quayCD";
            this.quayCD.Size = new System.Drawing.Size(75, 23);
            this.quayCD.TabIndex = 5;
            this.quayCD.Text = "Quay";
            this.quayCD.UseVisualStyleBackColor = true;
            this.quayCD.Visible = false;
            this.quayCD.Click += new System.EventHandler(this.Quay_Click);
            // 
            // tinhTienCD
            // 
            this.tinhTienCD.Location = new System.Drawing.Point(5, 160);
            this.tinhTienCD.Name = "tinhTienCD";
            this.tinhTienCD.Size = new System.Drawing.Size(75, 23);
            this.tinhTienCD.TabIndex = 5;
            this.tinhTienCD.Text = "Tịnh tiến";
            this.tinhTienCD.UseVisualStyleBackColor = true;
            this.tinhTienCD.Visible = false;
            this.tinhTienCD.Click += new System.EventHandler(this.TinhTienCD_Click);
            // 
            // hcn7
            // 
            this.hcn7.Location = new System.Drawing.Point(95, 255);
            this.hcn7.Name = "hcn7";
            this.hcn7.Size = new System.Drawing.Size(55, 20);
            this.hcn7.TabIndex = 4;
            this.hcn7.Text = "10";
            this.hcn7.Visible = false;
            // 
            // hcn3
            // 
            this.hcn3.Location = new System.Drawing.Point(95, 193);
            this.hcn3.Name = "hcn3";
            this.hcn3.Size = new System.Drawing.Size(55, 20);
            this.hcn3.TabIndex = 4;
            this.hcn3.Text = "0";
            this.hcn3.Visible = false;
            // 
            // hcn8
            // 
            this.hcn8.Location = new System.Drawing.Point(165, 256);
            this.hcn8.Name = "hcn8";
            this.hcn8.Size = new System.Drawing.Size(55, 20);
            this.hcn8.TabIndex = 4;
            this.hcn8.Text = "15";
            this.hcn8.Visible = false;
            // 
            // hcn4
            // 
            this.hcn4.Location = new System.Drawing.Point(165, 194);
            this.hcn4.Name = "hcn4";
            this.hcn4.Size = new System.Drawing.Size(55, 20);
            this.hcn4.TabIndex = 4;
            this.hcn4.Text = "0";
            this.hcn4.Visible = false;
            // 
            // hcn6
            // 
            this.hcn6.Location = new System.Drawing.Point(25, 255);
            this.hcn6.Name = "hcn6";
            this.hcn6.Size = new System.Drawing.Size(55, 20);
            this.hcn6.TabIndex = 4;
            this.hcn6.Text = "10";
            this.hcn6.Visible = false;
            // 
            // hcn2
            // 
            this.hcn2.Location = new System.Drawing.Point(25, 193);
            this.hcn2.Name = "hcn2";
            this.hcn2.Size = new System.Drawing.Size(55, 20);
            this.hcn2.TabIndex = 4;
            this.hcn2.Text = "0";
            this.hcn2.Visible = false;
            // 
            // hcn10
            // 
            this.hcn10.BackColor = System.Drawing.Color.Red;
            this.hcn10.Location = new System.Drawing.Point(77, 293);
            this.hcn10.Name = "hcn10";
            this.hcn10.Size = new System.Drawing.Size(88, 26);
            this.hcn10.TabIndex = 3;
            this.hcn10.Text = "VẼ HÌNH";
            this.hcn10.UseVisualStyleBackColor = false;
            this.hcn10.Visible = false;
            this.hcn10.Click += new System.EventHandler(this.hcn10_Click);
            // 
            // hcn9
            // 
            this.hcn9.BackColor = System.Drawing.Color.Red;
            this.hcn9.Location = new System.Drawing.Point(77, 292);
            this.hcn9.Name = "hcn9";
            this.hcn9.Size = new System.Drawing.Size(88, 26);
            this.hcn9.TabIndex = 3;
            this.hcn9.Text = "VẼ HÌNH";
            this.hcn9.UseVisualStyleBackColor = false;
            this.hcn9.Visible = false;
            this.hcn9.Click += new System.EventHandler(this.hcn9_Click);
            // 
            // hcn5
            // 
            this.hcn5.Location = new System.Drawing.Point(55, 231);
            this.hcn5.Name = "hcn5";
            this.hcn5.Size = new System.Drawing.Size(134, 23);
            this.hcn5.TabIndex = 3;
            this.hcn5.Text = "Chiều dài, cao, rộng:";
            this.hcn5.UseVisualStyleBackColor = true;
            this.hcn5.Visible = false;
            // 
            // hcn1
            // 
            this.hcn1.Location = new System.Drawing.Point(55, 169);
            this.hcn1.Name = "hcn1";
            this.hcn1.Size = new System.Drawing.Size(134, 23);
            this.hcn1.TabIndex = 3;
            this.hcn1.Text = "Tọa độ điểm gốc(x,y,z):";
            this.hcn1.UseVisualStyleBackColor = true;
            this.hcn1.Visible = false;
            // 
            // hinhTru
            // 
            this.hinhTru.Location = new System.Drawing.Point(124, 71);
            this.hinhTru.Name = "hinhTru";
            this.hinhTru.Size = new System.Drawing.Size(117, 23);
            this.hinhTru.TabIndex = 2;
            this.hinhTru.Text = "Hình Trụ Tròn";
            this.hinhTru.UseVisualStyleBackColor = true;
            this.hinhTru.Visible = false;
            this.hinhTru.Click += new System.EventHandler(this.hinhTru_Click);
            // 
            // hinhHCN
            // 
            this.hinhHCN.Location = new System.Drawing.Point(124, 42);
            this.hinhHCN.Name = "hinhHCN";
            this.hinhHCN.Size = new System.Drawing.Size(117, 23);
            this.hinhHCN.TabIndex = 2;
            this.hinhHCN.Text = "Hình Hộp Chữ Nhật";
            this.hinhHCN.UseVisualStyleBackColor = true;
            this.hinhHCN.Visible = false;
            this.hinhHCN.Click += new System.EventHandler(this.hinhHCN_Click);
            // 
            // chuyenDong2
            // 
            this.chuyenDong2.Location = new System.Drawing.Point(84, 120);
            this.chuyenDong2.Name = "chuyenDong2";
            this.chuyenDong2.Size = new System.Drawing.Size(81, 23);
            this.chuyenDong2.TabIndex = 1;
            this.chuyenDong2.Text = "Chuyển động";
            this.chuyenDong2.UseVisualStyleBackColor = true;
            this.chuyenDong2.Visible = false;
            this.chuyenDong2.Click += new System.EventHandler(this.ChuyenDong2_Click);
            // 
            // chuyenDong1
            // 
            this.chuyenDong1.Location = new System.Drawing.Point(84, 121);
            this.chuyenDong1.Name = "chuyenDong1";
            this.chuyenDong1.Size = new System.Drawing.Size(81, 23);
            this.chuyenDong1.TabIndex = 1;
            this.chuyenDong1.Text = "Chuyển động";
            this.chuyenDong1.UseVisualStyleBackColor = true;
            this.chuyenDong1.Visible = false;
            this.chuyenDong1.Click += new System.EventHandler(this.ChuyenDong1_Click);
            // 
            // hinh2
            // 
            this.hinh2.Location = new System.Drawing.Point(5, 71);
            this.hinh2.Name = "hinh2";
            this.hinh2.Size = new System.Drawing.Size(75, 23);
            this.hinh2.TabIndex = 1;
            this.hinh2.Text = "Vẽ hình 2";
            this.hinh2.UseVisualStyleBackColor = true;
            this.hinh2.Visible = false;
            this.hinh2.Click += new System.EventHandler(this.Hinh2_Click);
            // 
            // hinh1
            // 
            this.hinh1.Location = new System.Drawing.Point(5, 42);
            this.hinh1.Name = "hinh1";
            this.hinh1.Size = new System.Drawing.Size(75, 23);
            this.hinh1.TabIndex = 1;
            this.hinh1.Text = "Vẽ hình 1";
            this.hinh1.UseVisualStyleBackColor = true;
            this.hinh1.Visible = false;
            this.hinh1.Click += new System.EventHandler(this.Hinh1_Click);
            // 
            // ve3D
            // 
            this.ve3D.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ve3D.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ve3D.ForeColor = System.Drawing.Color.Red;
            this.ve3D.Location = new System.Drawing.Point(124, 3);
            this.ve3D.Name = "ve3D";
            this.ve3D.Size = new System.Drawing.Size(118, 32);
            this.ve3D.TabIndex = 0;
            this.ve3D.Text = "Vẽ 3D";
            this.ve3D.UseVisualStyleBackColor = false;
            this.ve3D.Click += new System.EventHandler(this.Ve3D_Click);
            // 
            // ve2D
            // 
            this.ve2D.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ve2D.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ve2D.ForeColor = System.Drawing.Color.Red;
            this.ve2D.Location = new System.Drawing.Point(5, 3);
            this.ve2D.Name = "ve2D";
            this.ve2D.Size = new System.Drawing.Size(118, 32);
            this.ve2D.TabIndex = 0;
            this.ve2D.Text = "Vẽ 2D";
            this.ve2D.UseVisualStyleBackColor = false;
            this.ve2D.Click += new System.EventHandler(this.Ve2D_Click);
            // 
            // toaDoCacDiem
            // 
            this.toaDoCacDiem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toaDoCacDiem.Controls.Add(this.labelh7);
            this.toaDoCacDiem.Controls.Add(this.labelh6);
            this.toaDoCacDiem.Controls.Add(this.labelh5);
            this.toaDoCacDiem.Controls.Add(this.labelh4);
            this.toaDoCacDiem.Controls.Add(this.labelh1);
            this.toaDoCacDiem.Controls.Add(this.labelh2);
            this.toaDoCacDiem.Controls.Add(this.labelh3);
            this.toaDoCacDiem.Controls.Add(this.label5);
            this.toaDoCacDiem.Controls.Add(this.d27);
            this.toaDoCacDiem.Controls.Add(this.d26);
            this.toaDoCacDiem.Controls.Add(this.d25);
            this.toaDoCacDiem.Controls.Add(this.d24);
            this.toaDoCacDiem.Controls.Add(this.d23);
            this.toaDoCacDiem.Controls.Add(this.d22);
            this.toaDoCacDiem.Controls.Add(this.d1);
            this.toaDoCacDiem.Controls.Add(this.d21);
            this.toaDoCacDiem.Controls.Add(this.d2);
            this.toaDoCacDiem.Controls.Add(this.d20);
            this.toaDoCacDiem.Controls.Add(this.d3);
            this.toaDoCacDiem.Controls.Add(this.d19);
            this.toaDoCacDiem.Controls.Add(this.d4);
            this.toaDoCacDiem.Controls.Add(this.d18);
            this.toaDoCacDiem.Controls.Add(this.d5);
            this.toaDoCacDiem.Controls.Add(this.d17);
            this.toaDoCacDiem.Controls.Add(this.d16);
            this.toaDoCacDiem.Controls.Add(this.d6);
            this.toaDoCacDiem.Controls.Add(this.d15);
            this.toaDoCacDiem.Controls.Add(this.d7);
            this.toaDoCacDiem.Controls.Add(this.d14);
            this.toaDoCacDiem.Controls.Add(this.d8);
            this.toaDoCacDiem.Controls.Add(this.d13);
            this.toaDoCacDiem.Controls.Add(this.d9);
            this.toaDoCacDiem.Controls.Add(this.d12);
            this.toaDoCacDiem.Controls.Add(this.d10);
            this.toaDoCacDiem.Controls.Add(this.d11);
            this.toaDoCacDiem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toaDoCacDiem.Location = new System.Drawing.Point(3, 345);
            this.toaDoCacDiem.Name = "toaDoCacDiem";
            this.toaDoCacDiem.Size = new System.Drawing.Size(248, 337);
            this.toaDoCacDiem.TabIndex = 1;
            this.toaDoCacDiem.Paint += new System.Windows.Forms.PaintEventHandler(this.toaDoCacDiem_Paint);
            // 
            // labelh7
            // 
            this.labelh7.AutoSize = true;
            this.labelh7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh7.Location = new System.Drawing.Point(5, 309);
            this.labelh7.Name = "labelh7";
            this.labelh7.Size = new System.Drawing.Size(41, 15);
            this.labelh7.TabIndex = 2;
            this.labelh7.Text = "label1";
            this.labelh7.Visible = false;
            // 
            // labelh6
            // 
            this.labelh6.AutoSize = true;
            this.labelh6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh6.Location = new System.Drawing.Point(5, 290);
            this.labelh6.Name = "labelh6";
            this.labelh6.Size = new System.Drawing.Size(41, 15);
            this.labelh6.TabIndex = 2;
            this.labelh6.Text = "label1";
            this.labelh6.Visible = false;
            // 
            // labelh5
            // 
            this.labelh5.AutoSize = true;
            this.labelh5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh5.Location = new System.Drawing.Point(5, 272);
            this.labelh5.Name = "labelh5";
            this.labelh5.Size = new System.Drawing.Size(41, 15);
            this.labelh5.TabIndex = 2;
            this.labelh5.Text = "label1";
            this.labelh5.Visible = false;
            // 
            // labelh4
            // 
            this.labelh4.AutoSize = true;
            this.labelh4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh4.Location = new System.Drawing.Point(5, 254);
            this.labelh4.Name = "labelh4";
            this.labelh4.Size = new System.Drawing.Size(41, 15);
            this.labelh4.TabIndex = 2;
            this.labelh4.Text = "label1";
            this.labelh4.Visible = false;
            // 
            // labelh1
            // 
            this.labelh1.AutoSize = true;
            this.labelh1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh1.Location = new System.Drawing.Point(5, 199);
            this.labelh1.Name = "labelh1";
            this.labelh1.Size = new System.Drawing.Size(41, 15);
            this.labelh1.TabIndex = 2;
            this.labelh1.Text = "label1";
            this.labelh1.Visible = false;
            // 
            // labelh2
            // 
            this.labelh2.AutoSize = true;
            this.labelh2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh2.Location = new System.Drawing.Point(5, 217);
            this.labelh2.Name = "labelh2";
            this.labelh2.Size = new System.Drawing.Size(41, 15);
            this.labelh2.TabIndex = 2;
            this.labelh2.Text = "label1";
            this.labelh2.Visible = false;
            // 
            // labelh3
            // 
            this.labelh3.AutoSize = true;
            this.labelh3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelh3.Location = new System.Drawing.Point(5, 236);
            this.labelh3.Name = "labelh3";
            this.labelh3.Size = new System.Drawing.Size(41, 15);
            this.labelh3.TabIndex = 2;
            this.labelh3.Text = "label1";
            this.labelh3.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(21, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(187, 22);
            this.label5.TabIndex = 1;
            this.label5.Text = "TỌA ĐỘ CÁC ĐIỂM";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // d27
            // 
            this.d27.AutoSize = true;
            this.d27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d27.Location = new System.Drawing.Point(158, 164);
            this.d27.Name = "d27";
            this.d27.Size = new System.Drawing.Size(14, 15);
            this.d27.TabIndex = 0;
            this.d27.Text = "a";
            this.d27.Visible = false;
            // 
            // d26
            // 
            this.d26.AutoSize = true;
            this.d26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d26.Location = new System.Drawing.Point(158, 147);
            this.d26.Name = "d26";
            this.d26.Size = new System.Drawing.Size(14, 15);
            this.d26.TabIndex = 0;
            this.d26.Text = "Z";
            this.d26.Visible = false;
            // 
            // d25
            // 
            this.d25.AutoSize = true;
            this.d25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d25.Location = new System.Drawing.Point(158, 130);
            this.d25.Name = "d25";
            this.d25.Size = new System.Drawing.Size(14, 15);
            this.d25.TabIndex = 0;
            this.d25.Text = "Y";
            this.d25.Visible = false;
            // 
            // d24
            // 
            this.d24.AutoSize = true;
            this.d24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d24.Location = new System.Drawing.Point(158, 113);
            this.d24.Name = "d24";
            this.d24.Size = new System.Drawing.Size(15, 15);
            this.d24.TabIndex = 0;
            this.d24.Text = "X";
            this.d24.Visible = false;
            // 
            // d23
            // 
            this.d23.AutoSize = true;
            this.d23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d23.Location = new System.Drawing.Point(158, 96);
            this.d23.Name = "d23";
            this.d23.Size = new System.Drawing.Size(18, 15);
            this.d23.TabIndex = 0;
            this.d23.Text = "W";
            this.d23.Visible = false;
            // 
            // d22
            // 
            this.d22.AutoSize = true;
            this.d22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d22.Location = new System.Drawing.Point(158, 79);
            this.d22.Name = "d22";
            this.d22.Size = new System.Drawing.Size(14, 15);
            this.d22.TabIndex = 0;
            this.d22.Text = "V";
            this.d22.Visible = false;
            // 
            // d1
            // 
            this.d1.AutoSize = true;
            this.d1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d1.Location = new System.Drawing.Point(5, 28);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(14, 15);
            this.d1.TabIndex = 0;
            this.d1.Text = "A";
            this.d1.Visible = false;
            // 
            // d21
            // 
            this.d21.AutoSize = true;
            this.d21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d21.Location = new System.Drawing.Point(158, 62);
            this.d21.Name = "d21";
            this.d21.Size = new System.Drawing.Size(16, 15);
            this.d21.TabIndex = 0;
            this.d21.Text = "U";
            this.d21.Visible = false;
            // 
            // d2
            // 
            this.d2.AutoSize = true;
            this.d2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d2.Location = new System.Drawing.Point(5, 45);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(15, 15);
            this.d2.TabIndex = 0;
            this.d2.Text = "B";
            this.d2.Visible = false;
            // 
            // d20
            // 
            this.d20.AutoSize = true;
            this.d20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d20.Location = new System.Drawing.Point(158, 45);
            this.d20.Name = "d20";
            this.d20.Size = new System.Drawing.Size(14, 15);
            this.d20.TabIndex = 0;
            this.d20.Text = "T";
            this.d20.Visible = false;
            // 
            // d3
            // 
            this.d3.AutoSize = true;
            this.d3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d3.Location = new System.Drawing.Point(5, 62);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(15, 15);
            this.d3.TabIndex = 0;
            this.d3.Text = "C";
            this.d3.Visible = false;
            // 
            // d19
            // 
            this.d19.AutoSize = true;
            this.d19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d19.Location = new System.Drawing.Point(158, 28);
            this.d19.Name = "d19";
            this.d19.Size = new System.Drawing.Size(15, 15);
            this.d19.TabIndex = 0;
            this.d19.Text = "S";
            this.d19.Visible = false;
            // 
            // d4
            // 
            this.d4.AutoSize = true;
            this.d4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d4.Location = new System.Drawing.Point(5, 79);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(16, 15);
            this.d4.TabIndex = 0;
            this.d4.Text = "D";
            this.d4.Visible = false;
            // 
            // d18
            // 
            this.d18.AutoSize = true;
            this.d18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d18.Location = new System.Drawing.Point(76, 164);
            this.d18.Name = "d18";
            this.d18.Size = new System.Drawing.Size(16, 15);
            this.d18.TabIndex = 0;
            this.d18.Text = "R";
            this.d18.Visible = false;
            // 
            // d5
            // 
            this.d5.AutoSize = true;
            this.d5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d5.Location = new System.Drawing.Point(5, 96);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(15, 15);
            this.d5.TabIndex = 0;
            this.d5.Text = "E";
            this.d5.Visible = false;
            // 
            // d17
            // 
            this.d17.AutoSize = true;
            this.d17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d17.Location = new System.Drawing.Point(76, 147);
            this.d17.Name = "d17";
            this.d17.Size = new System.Drawing.Size(16, 15);
            this.d17.TabIndex = 0;
            this.d17.Text = "Q";
            this.d17.Visible = false;
            // 
            // d16
            // 
            this.d16.AutoSize = true;
            this.d16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d16.Location = new System.Drawing.Point(76, 130);
            this.d16.Name = "d16";
            this.d16.Size = new System.Drawing.Size(15, 15);
            this.d16.TabIndex = 0;
            this.d16.Text = "P";
            this.d16.Visible = false;
            // 
            // d6
            // 
            this.d6.AutoSize = true;
            this.d6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d6.Location = new System.Drawing.Point(5, 113);
            this.d6.Name = "d6";
            this.d6.Size = new System.Drawing.Size(14, 15);
            this.d6.TabIndex = 0;
            this.d6.Text = "F";
            this.d6.Visible = false;
            // 
            // d15
            // 
            this.d15.AutoSize = true;
            this.d15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d15.Location = new System.Drawing.Point(76, 113);
            this.d15.Name = "d15";
            this.d15.Size = new System.Drawing.Size(16, 15);
            this.d15.TabIndex = 0;
            this.d15.Text = "O";
            this.d15.Visible = false;
            // 
            // d7
            // 
            this.d7.AutoSize = true;
            this.d7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d7.Location = new System.Drawing.Point(5, 130);
            this.d7.Name = "d7";
            this.d7.Size = new System.Drawing.Size(16, 15);
            this.d7.TabIndex = 0;
            this.d7.Text = "G";
            this.d7.Visible = false;
            // 
            // d14
            // 
            this.d14.AutoSize = true;
            this.d14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d14.Location = new System.Drawing.Point(76, 96);
            this.d14.Name = "d14";
            this.d14.Size = new System.Drawing.Size(16, 15);
            this.d14.TabIndex = 0;
            this.d14.Text = "N";
            this.d14.Visible = false;
            // 
            // d8
            // 
            this.d8.AutoSize = true;
            this.d8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d8.Location = new System.Drawing.Point(5, 147);
            this.d8.Name = "d8";
            this.d8.Size = new System.Drawing.Size(16, 15);
            this.d8.TabIndex = 0;
            this.d8.Text = "H";
            this.d8.Visible = false;
            // 
            // d13
            // 
            this.d13.AutoSize = true;
            this.d13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d13.Location = new System.Drawing.Point(76, 79);
            this.d13.Name = "d13";
            this.d13.Size = new System.Drawing.Size(18, 15);
            this.d13.TabIndex = 0;
            this.d13.Text = "M";
            this.d13.Visible = false;
            // 
            // d9
            // 
            this.d9.AutoSize = true;
            this.d9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d9.Location = new System.Drawing.Point(5, 164);
            this.d9.Name = "d9";
            this.d9.Size = new System.Drawing.Size(10, 15);
            this.d9.TabIndex = 0;
            this.d9.Text = "I";
            this.d9.Visible = false;
            // 
            // d12
            // 
            this.d12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.d12.AutoSize = true;
            this.d12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d12.Location = new System.Drawing.Point(76, 62);
            this.d12.Name = "d12";
            this.d12.Size = new System.Drawing.Size(14, 15);
            this.d12.TabIndex = 0;
            this.d12.Text = "L";
            this.d12.Visible = false;
            // 
            // d10
            // 
            this.d10.AutoSize = true;
            this.d10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d10.Location = new System.Drawing.Point(76, 28);
            this.d10.Name = "d10";
            this.d10.Size = new System.Drawing.Size(13, 15);
            this.d10.TabIndex = 0;
            this.d10.Text = "J";
            this.d10.Visible = false;
            // 
            // d11
            // 
            this.d11.AutoSize = true;
            this.d11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.d11.Location = new System.Drawing.Point(76, 45);
            this.d11.Name = "d11";
            this.d11.Size = new System.Drawing.Size(15, 15);
            this.d11.TabIndex = 0;
            this.d11.Text = "K";
            this.d11.Visible = false;
            // 
            // heTrucToaDo
            // 
            this.heTrucToaDo.BackColor = System.Drawing.Color.White;
            this.heTrucToaDo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.heTrucToaDo.Controls.Add(this.t1);
            this.heTrucToaDo.Controls.Add(this.t35);
            this.heTrucToaDo.Controls.Add(this.t2);
            this.heTrucToaDo.Controls.Add(this.t36);
            this.heTrucToaDo.Controls.Add(this.t3);
            this.heTrucToaDo.Controls.Add(this.t37);
            this.heTrucToaDo.Controls.Add(this.t4);
            this.heTrucToaDo.Controls.Add(this.t38);
            this.heTrucToaDo.Controls.Add(this.t5);
            this.heTrucToaDo.Controls.Add(this.t39);
            this.heTrucToaDo.Controls.Add(this.t6);
            this.heTrucToaDo.Controls.Add(this.t40);
            this.heTrucToaDo.Controls.Add(this.t7);
            this.heTrucToaDo.Controls.Add(this.t8);
            this.heTrucToaDo.Controls.Add(this.t9);
            this.heTrucToaDo.Controls.Add(this.t10);
            this.heTrucToaDo.Controls.Add(this.t41);
            this.heTrucToaDo.Controls.Add(this.t11);
            this.heTrucToaDo.Controls.Add(this.t42);
            this.heTrucToaDo.Controls.Add(this.t12);
            this.heTrucToaDo.Controls.Add(this.t43);
            this.heTrucToaDo.Controls.Add(this.t13);
            this.heTrucToaDo.Controls.Add(this.t34);
            this.heTrucToaDo.Controls.Add(this.t33);
            this.heTrucToaDo.Controls.Add(this.t32);
            this.heTrucToaDo.Controls.Add(this.t31);
            this.heTrucToaDo.Controls.Add(this.t30);
            this.heTrucToaDo.Controls.Add(this.t29);
            this.heTrucToaDo.Controls.Add(this.t28);
            this.heTrucToaDo.Controls.Add(this.t27);
            this.heTrucToaDo.Controls.Add(this.t26);
            this.heTrucToaDo.Controls.Add(this.t25);
            this.heTrucToaDo.Controls.Add(this.t24);
            this.heTrucToaDo.Controls.Add(this.t23);
            this.heTrucToaDo.Controls.Add(this.t22);
            this.heTrucToaDo.Controls.Add(this.t21);
            this.heTrucToaDo.Controls.Add(this.t20);
            this.heTrucToaDo.Controls.Add(this.t19);
            this.heTrucToaDo.Controls.Add(this.t18);
            this.heTrucToaDo.Controls.Add(this.t17);
            this.heTrucToaDo.Controls.Add(this.t46);
            this.heTrucToaDo.Controls.Add(this.t16);
            this.heTrucToaDo.Controls.Add(this.t45);
            this.heTrucToaDo.Controls.Add(this.t15);
            this.heTrucToaDo.Controls.Add(this.t44);
            this.heTrucToaDo.Controls.Add(this.t14);
            this.heTrucToaDo.Controls.Add(this.dV);
            this.heTrucToaDo.Controls.Add(this.dU);
            this.heTrucToaDo.Controls.Add(this.dT);
            this.heTrucToaDo.Controls.Add(this.dS);
            this.heTrucToaDo.Controls.Add(this.dR);
            this.heTrucToaDo.Controls.Add(this.dQ);
            this.heTrucToaDo.Controls.Add(this.diemP);
            this.heTrucToaDo.Controls.Add(this.diemO);
            this.heTrucToaDo.Controls.Add(this.diemN);
            this.heTrucToaDo.Controls.Add(this.diemM);
            this.heTrucToaDo.Controls.Add(this.diemL);
            this.heTrucToaDo.Controls.Add(this.diemK);
            this.heTrucToaDo.Controls.Add(this.diemJ);
            this.heTrucToaDo.Controls.Add(this.diemI);
            this.heTrucToaDo.Controls.Add(this.diemH);
            this.heTrucToaDo.Controls.Add(this.diemG);
            this.heTrucToaDo.Controls.Add(this.diemF);
            this.heTrucToaDo.Controls.Add(this.diemE);
            this.heTrucToaDo.Controls.Add(this.diemD);
            this.heTrucToaDo.Controls.Add(this.diemC);
            this.heTrucToaDo.Controls.Add(this.diemB);
            this.heTrucToaDo.Controls.Add(this.da);
            this.heTrucToaDo.Controls.Add(this.dZ);
            this.heTrucToaDo.Controls.Add(this.dY);
            this.heTrucToaDo.Controls.Add(this.dX);
            this.heTrucToaDo.Controls.Add(this.dW);
            this.heTrucToaDo.Controls.Add(this.diemA);
            this.heTrucToaDo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.heTrucToaDo.Location = new System.Drawing.Point(263, 3);
            this.heTrucToaDo.Name = "heTrucToaDo";
            this.heTrucToaDo.Size = new System.Drawing.Size(1018, 685);
            this.heTrucToaDo.TabIndex = 1;
            this.heTrucToaDo.Visible = false;
            this.heTrucToaDo.Paint += new System.Windows.Forms.PaintEventHandler(this.HeTrucToaDo_Paint);
            // 
            // t1
            // 
            this.t1.AutoSize = true;
            this.t1.Location = new System.Drawing.Point(503, 353);
            this.t1.Name = "t1";
            this.t1.Size = new System.Drawing.Size(13, 13);
            this.t1.TabIndex = 1;
            this.t1.Text = "0";
            this.t1.Visible = false;
            // 
            // t35
            // 
            this.t35.AutoSize = true;
            this.t35.Location = new System.Drawing.Point(454, 401);
            this.t35.Name = "t35";
            this.t35.Size = new System.Drawing.Size(19, 13);
            this.t35.TabIndex = 1;
            this.t35.Text = "10";
            this.t35.Visible = false;
            // 
            // t2
            // 
            this.t2.AutoSize = true;
            this.t2.Location = new System.Drawing.Point(545, 353);
            this.t2.Name = "t2";
            this.t2.Size = new System.Drawing.Size(13, 13);
            this.t2.TabIndex = 1;
            this.t2.Text = "5";
            this.t2.Visible = false;
            // 
            // t36
            // 
            this.t36.AutoSize = true;
            this.t36.Location = new System.Drawing.Point(404, 451);
            this.t36.Name = "t36";
            this.t36.Size = new System.Drawing.Size(19, 13);
            this.t36.TabIndex = 1;
            this.t36.Text = "20";
            this.t36.Visible = false;
            // 
            // t3
            // 
            this.t3.AutoSize = true;
            this.t3.Location = new System.Drawing.Point(590, 353);
            this.t3.Name = "t3";
            this.t3.Size = new System.Drawing.Size(19, 13);
            this.t3.TabIndex = 1;
            this.t3.Text = "10";
            this.t3.Visible = false;
            // 
            // t37
            // 
            this.t37.AutoSize = true;
            this.t37.Location = new System.Drawing.Point(354, 501);
            this.t37.Name = "t37";
            this.t37.Size = new System.Drawing.Size(19, 13);
            this.t37.TabIndex = 1;
            this.t37.Text = "30";
            this.t37.Visible = false;
            // 
            // t4
            // 
            this.t4.AutoSize = true;
            this.t4.Location = new System.Drawing.Point(640, 353);
            this.t4.Name = "t4";
            this.t4.Size = new System.Drawing.Size(19, 13);
            this.t4.TabIndex = 1;
            this.t4.Text = "15";
            this.t4.Visible = false;
            // 
            // t38
            // 
            this.t38.AutoSize = true;
            this.t38.Location = new System.Drawing.Point(304, 551);
            this.t38.Name = "t38";
            this.t38.Size = new System.Drawing.Size(19, 13);
            this.t38.TabIndex = 1;
            this.t38.Text = "40";
            this.t38.Visible = false;
            // 
            // t5
            // 
            this.t5.AutoSize = true;
            this.t5.Location = new System.Drawing.Point(690, 353);
            this.t5.Name = "t5";
            this.t5.Size = new System.Drawing.Size(19, 13);
            this.t5.TabIndex = 1;
            this.t5.Text = "20";
            this.t5.Visible = false;
            // 
            // t39
            // 
            this.t39.AutoSize = true;
            this.t39.Location = new System.Drawing.Point(254, 601);
            this.t39.Name = "t39";
            this.t39.Size = new System.Drawing.Size(19, 13);
            this.t39.TabIndex = 1;
            this.t39.Text = "50";
            this.t39.Visible = false;
            // 
            // t6
            // 
            this.t6.AutoSize = true;
            this.t6.Location = new System.Drawing.Point(740, 353);
            this.t6.Name = "t6";
            this.t6.Size = new System.Drawing.Size(19, 13);
            this.t6.TabIndex = 1;
            this.t6.Text = "25";
            this.t6.Visible = false;
            // 
            // t40
            // 
            this.t40.AutoSize = true;
            this.t40.Location = new System.Drawing.Point(204, 651);
            this.t40.Name = "t40";
            this.t40.Size = new System.Drawing.Size(19, 13);
            this.t40.TabIndex = 1;
            this.t40.Text = "60";
            this.t40.Visible = false;
            // 
            // t7
            // 
            this.t7.AutoSize = true;
            this.t7.Location = new System.Drawing.Point(790, 353);
            this.t7.Name = "t7";
            this.t7.Size = new System.Drawing.Size(19, 13);
            this.t7.TabIndex = 1;
            this.t7.Text = "30";
            this.t7.Visible = false;
            // 
            // t8
            // 
            this.t8.AutoSize = true;
            this.t8.Location = new System.Drawing.Point(840, 353);
            this.t8.Name = "t8";
            this.t8.Size = new System.Drawing.Size(19, 13);
            this.t8.TabIndex = 1;
            this.t8.Text = "35";
            this.t8.Visible = false;
            // 
            // t9
            // 
            this.t9.AutoSize = true;
            this.t9.Location = new System.Drawing.Point(890, 353);
            this.t9.Name = "t9";
            this.t9.Size = new System.Drawing.Size(19, 13);
            this.t9.TabIndex = 1;
            this.t9.Text = "40";
            this.t9.Visible = false;
            // 
            // t10
            // 
            this.t10.AutoSize = true;
            this.t10.Location = new System.Drawing.Point(940, 353);
            this.t10.Name = "t10";
            this.t10.Size = new System.Drawing.Size(19, 13);
            this.t10.TabIndex = 1;
            this.t10.Text = "45";
            this.t10.Visible = false;
            // 
            // t41
            // 
            this.t41.AutoSize = true;
            this.t41.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t41.Location = new System.Drawing.Point(553, 303);
            this.t41.Name = "t41";
            this.t41.Size = new System.Drawing.Size(22, 13);
            this.t41.TabIndex = 1;
            this.t41.Text = "-10";
            this.t41.Visible = false;
            // 
            // t11
            // 
            this.t11.AutoSize = true;
            this.t11.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t11.Location = new System.Drawing.Point(443, 353);
            this.t11.Name = "t11";
            this.t11.Size = new System.Drawing.Size(16, 13);
            this.t11.TabIndex = 1;
            this.t11.Text = "-5";
            this.t11.Visible = false;
            // 
            // t42
            // 
            this.t42.AutoSize = true;
            this.t42.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t42.Location = new System.Drawing.Point(603, 253);
            this.t42.Name = "t42";
            this.t42.Size = new System.Drawing.Size(22, 13);
            this.t42.TabIndex = 1;
            this.t42.Text = "-20";
            this.t42.Visible = false;
            // 
            // t12
            // 
            this.t12.AutoSize = true;
            this.t12.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t12.Location = new System.Drawing.Point(390, 353);
            this.t12.Name = "t12";
            this.t12.Size = new System.Drawing.Size(22, 13);
            this.t12.TabIndex = 1;
            this.t12.Text = "-10";
            this.t12.Visible = false;
            // 
            // t43
            // 
            this.t43.AutoSize = true;
            this.t43.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t43.Location = new System.Drawing.Point(653, 203);
            this.t43.Name = "t43";
            this.t43.Size = new System.Drawing.Size(22, 13);
            this.t43.TabIndex = 1;
            this.t43.Text = "-30";
            this.t43.Visible = false;
            // 
            // t13
            // 
            this.t13.AutoSize = true;
            this.t13.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t13.Location = new System.Drawing.Point(340, 353);
            this.t13.Name = "t13";
            this.t13.Size = new System.Drawing.Size(22, 13);
            this.t13.TabIndex = 1;
            this.t13.Text = "-15";
            this.t13.Visible = false;
            // 
            // t34
            // 
            this.t34.AutoSize = true;
            this.t34.Location = new System.Drawing.Point(155, 665);
            this.t34.Name = "t34";
            this.t34.Size = new System.Drawing.Size(14, 13);
            this.t34.TabIndex = 1;
            this.t34.Text = "Z";
            this.t34.Visible = false;
            // 
            // t33
            // 
            this.t33.AutoSize = true;
            this.t33.Location = new System.Drawing.Point(506, 3);
            this.t33.Name = "t33";
            this.t33.Size = new System.Drawing.Size(14, 13);
            this.t33.TabIndex = 1;
            this.t33.Text = "Y";
            this.t33.Visible = false;
            // 
            // t32
            // 
            this.t32.AutoSize = true;
            this.t32.Location = new System.Drawing.Point(995, 356);
            this.t32.Name = "t32";
            this.t32.Size = new System.Drawing.Size(14, 13);
            this.t32.TabIndex = 1;
            this.t32.Text = "X";
            this.t32.Visible = false;
            // 
            // t31
            // 
            this.t31.AutoSize = true;
            this.t31.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t31.Location = new System.Drawing.Point(503, 643);
            this.t31.Name = "t31";
            this.t31.Size = new System.Drawing.Size(22, 13);
            this.t31.TabIndex = 1;
            this.t31.Text = "-30";
            this.t31.Visible = false;
            // 
            // t30
            // 
            this.t30.AutoSize = true;
            this.t30.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t30.Location = new System.Drawing.Point(503, 593);
            this.t30.Name = "t30";
            this.t30.Size = new System.Drawing.Size(22, 13);
            this.t30.TabIndex = 1;
            this.t30.Text = "-25";
            this.t30.Visible = false;
            // 
            // t29
            // 
            this.t29.AutoSize = true;
            this.t29.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t29.Location = new System.Drawing.Point(503, 543);
            this.t29.Name = "t29";
            this.t29.Size = new System.Drawing.Size(22, 13);
            this.t29.TabIndex = 1;
            this.t29.Text = "-20";
            this.t29.Visible = false;
            // 
            // t28
            // 
            this.t28.AutoSize = true;
            this.t28.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t28.Location = new System.Drawing.Point(503, 493);
            this.t28.Name = "t28";
            this.t28.Size = new System.Drawing.Size(22, 13);
            this.t28.TabIndex = 1;
            this.t28.Text = "-15";
            this.t28.Visible = false;
            // 
            // t27
            // 
            this.t27.AutoSize = true;
            this.t27.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t27.Location = new System.Drawing.Point(503, 443);
            this.t27.Name = "t27";
            this.t27.Size = new System.Drawing.Size(22, 13);
            this.t27.TabIndex = 1;
            this.t27.Text = "-10";
            this.t27.Visible = false;
            // 
            // t26
            // 
            this.t26.AutoSize = true;
            this.t26.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t26.Location = new System.Drawing.Point(503, 393);
            this.t26.Name = "t26";
            this.t26.Size = new System.Drawing.Size(16, 13);
            this.t26.TabIndex = 1;
            this.t26.Text = "-5";
            this.t26.Visible = false;
            // 
            // t25
            // 
            this.t25.AutoSize = true;
            this.t25.Location = new System.Drawing.Point(503, 44);
            this.t25.Name = "t25";
            this.t25.Size = new System.Drawing.Size(19, 13);
            this.t25.TabIndex = 1;
            this.t25.Text = "30";
            this.t25.Visible = false;
            // 
            // t24
            // 
            this.t24.AutoSize = true;
            this.t24.Location = new System.Drawing.Point(503, 94);
            this.t24.Name = "t24";
            this.t24.Size = new System.Drawing.Size(19, 13);
            this.t24.TabIndex = 1;
            this.t24.Text = "25";
            this.t24.Visible = false;
            // 
            // t23
            // 
            this.t23.AutoSize = true;
            this.t23.Location = new System.Drawing.Point(503, 144);
            this.t23.Name = "t23";
            this.t23.Size = new System.Drawing.Size(19, 13);
            this.t23.TabIndex = 1;
            this.t23.Text = "20";
            this.t23.Visible = false;
            // 
            // t22
            // 
            this.t22.AutoSize = true;
            this.t22.Location = new System.Drawing.Point(503, 193);
            this.t22.Name = "t22";
            this.t22.Size = new System.Drawing.Size(19, 13);
            this.t22.TabIndex = 1;
            this.t22.Text = "15";
            this.t22.Visible = false;
            // 
            // t21
            // 
            this.t21.AutoSize = true;
            this.t21.Location = new System.Drawing.Point(503, 244);
            this.t21.Name = "t21";
            this.t21.Size = new System.Drawing.Size(19, 13);
            this.t21.TabIndex = 1;
            this.t21.Text = "10";
            this.t21.Visible = false;
            // 
            // t20
            // 
            this.t20.AutoSize = true;
            this.t20.Location = new System.Drawing.Point(503, 295);
            this.t20.Name = "t20";
            this.t20.Size = new System.Drawing.Size(13, 13);
            this.t20.TabIndex = 1;
            this.t20.Text = "5";
            this.t20.Visible = false;
            // 
            // t19
            // 
            this.t19.AutoSize = true;
            this.t19.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t19.Location = new System.Drawing.Point(40, 353);
            this.t19.Name = "t19";
            this.t19.Size = new System.Drawing.Size(22, 13);
            this.t19.TabIndex = 1;
            this.t19.Text = "-45";
            this.t19.Visible = false;
            // 
            // t18
            // 
            this.t18.AutoSize = true;
            this.t18.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t18.Location = new System.Drawing.Point(90, 353);
            this.t18.Name = "t18";
            this.t18.Size = new System.Drawing.Size(22, 13);
            this.t18.TabIndex = 1;
            this.t18.Text = "-40";
            this.t18.Visible = false;
            // 
            // t17
            // 
            this.t17.AutoSize = true;
            this.t17.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t17.Location = new System.Drawing.Point(140, 353);
            this.t17.Name = "t17";
            this.t17.Size = new System.Drawing.Size(22, 13);
            this.t17.TabIndex = 1;
            this.t17.Text = "-35";
            this.t17.Visible = false;
            // 
            // t46
            // 
            this.t46.AutoSize = true;
            this.t46.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t46.Location = new System.Drawing.Point(803, 53);
            this.t46.Name = "t46";
            this.t46.Size = new System.Drawing.Size(22, 13);
            this.t46.TabIndex = 1;
            this.t46.Text = "-60";
            this.t46.Visible = false;
            // 
            // t16
            // 
            this.t16.AutoSize = true;
            this.t16.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t16.Location = new System.Drawing.Point(190, 353);
            this.t16.Name = "t16";
            this.t16.Size = new System.Drawing.Size(22, 13);
            this.t16.TabIndex = 1;
            this.t16.Text = "-30";
            this.t16.Visible = false;
            // 
            // t45
            // 
            this.t45.AutoSize = true;
            this.t45.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t45.Location = new System.Drawing.Point(753, 103);
            this.t45.Name = "t45";
            this.t45.Size = new System.Drawing.Size(22, 13);
            this.t45.TabIndex = 1;
            this.t45.Text = "-50";
            this.t45.Visible = false;
            // 
            // t15
            // 
            this.t15.AutoSize = true;
            this.t15.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t15.Location = new System.Drawing.Point(240, 353);
            this.t15.Name = "t15";
            this.t15.Size = new System.Drawing.Size(22, 13);
            this.t15.TabIndex = 1;
            this.t15.Text = "-25";
            this.t15.Visible = false;
            // 
            // t44
            // 
            this.t44.AutoSize = true;
            this.t44.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t44.Location = new System.Drawing.Point(703, 153);
            this.t44.Name = "t44";
            this.t44.Size = new System.Drawing.Size(22, 13);
            this.t44.TabIndex = 1;
            this.t44.Text = "-40";
            this.t44.Visible = false;
            // 
            // t14
            // 
            this.t14.AutoSize = true;
            this.t14.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.t14.Location = new System.Drawing.Point(290, 353);
            this.t14.Name = "t14";
            this.t14.Size = new System.Drawing.Size(22, 13);
            this.t14.TabIndex = 1;
            this.t14.Text = "-20";
            this.t14.Visible = false;
            // 
            // dV
            // 
            this.dV.AutoSize = true;
            this.dV.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dV.Location = new System.Drawing.Point(565, 7);
            this.dV.Name = "dV";
            this.dV.Size = new System.Drawing.Size(10, 9);
            this.dV.TabIndex = 0;
            this.dV.Text = "V";
            this.dV.Visible = false;
            // 
            // dU
            // 
            this.dU.AutoSize = true;
            this.dU.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dU.Location = new System.Drawing.Point(534, 7);
            this.dU.Name = "dU";
            this.dU.Size = new System.Drawing.Size(11, 9);
            this.dU.TabIndex = 0;
            this.dU.Text = "U";
            this.dU.Visible = false;
            // 
            // dT
            // 
            this.dT.AutoSize = true;
            this.dT.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dT.Location = new System.Drawing.Point(514, 7);
            this.dT.Name = "dT";
            this.dT.Size = new System.Drawing.Size(10, 9);
            this.dT.TabIndex = 0;
            this.dT.Text = "T";
            this.dT.Visible = false;
            // 
            // dS
            // 
            this.dS.AutoSize = true;
            this.dS.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dS.Location = new System.Drawing.Point(478, 7);
            this.dS.Name = "dS";
            this.dS.Size = new System.Drawing.Size(10, 9);
            this.dS.TabIndex = 0;
            this.dS.Text = "S";
            this.dS.Visible = false;
            // 
            // dR
            // 
            this.dR.AutoSize = true;
            this.dR.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dR.Location = new System.Drawing.Point(439, 7);
            this.dR.Name = "dR";
            this.dR.Size = new System.Drawing.Size(11, 9);
            this.dR.TabIndex = 0;
            this.dR.Text = "R";
            this.dR.Visible = false;
            // 
            // dQ
            // 
            this.dQ.AutoSize = true;
            this.dQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dQ.Location = new System.Drawing.Point(419, 7);
            this.dQ.Name = "dQ";
            this.dQ.Size = new System.Drawing.Size(11, 9);
            this.dQ.TabIndex = 0;
            this.dQ.Text = "Q";
            this.dQ.Visible = false;
            // 
            // diemP
            // 
            this.diemP.AutoSize = true;
            this.diemP.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemP.Location = new System.Drawing.Point(389, 7);
            this.diemP.Name = "diemP";
            this.diemP.Size = new System.Drawing.Size(10, 9);
            this.diemP.TabIndex = 0;
            this.diemP.Text = "P";
            this.diemP.Visible = false;
            // 
            // diemO
            // 
            this.diemO.AutoSize = true;
            this.diemO.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemO.Location = new System.Drawing.Point(369, 7);
            this.diemO.Name = "diemO";
            this.diemO.Size = new System.Drawing.Size(11, 9);
            this.diemO.TabIndex = 0;
            this.diemO.Text = "O";
            this.diemO.Visible = false;
            // 
            // diemN
            // 
            this.diemN.AutoSize = true;
            this.diemN.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemN.Location = new System.Drawing.Point(349, 7);
            this.diemN.Name = "diemN";
            this.diemN.Size = new System.Drawing.Size(11, 9);
            this.diemN.TabIndex = 0;
            this.diemN.Text = "N";
            this.diemN.Visible = false;
            // 
            // diemM
            // 
            this.diemM.AutoSize = true;
            this.diemM.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemM.Location = new System.Drawing.Point(329, 7);
            this.diemM.Name = "diemM";
            this.diemM.Size = new System.Drawing.Size(12, 9);
            this.diemM.TabIndex = 0;
            this.diemM.Text = "M";
            this.diemM.Visible = false;
            // 
            // diemL
            // 
            this.diemL.AutoSize = true;
            this.diemL.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemL.Location = new System.Drawing.Point(309, 7);
            this.diemL.Name = "diemL";
            this.diemL.Size = new System.Drawing.Size(9, 9);
            this.diemL.TabIndex = 0;
            this.diemL.Text = "L";
            this.diemL.Visible = false;
            // 
            // diemK
            // 
            this.diemK.AutoSize = true;
            this.diemK.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemK.Location = new System.Drawing.Point(271, 7);
            this.diemK.Name = "diemK";
            this.diemK.Size = new System.Drawing.Size(10, 9);
            this.diemK.TabIndex = 0;
            this.diemK.Text = "K";
            this.diemK.Visible = false;
            // 
            // diemJ
            // 
            this.diemJ.AutoSize = true;
            this.diemJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemJ.Location = new System.Drawing.Point(251, 7);
            this.diemJ.Name = "diemJ";
            this.diemJ.Size = new System.Drawing.Size(9, 9);
            this.diemJ.TabIndex = 0;
            this.diemJ.Text = "J";
            this.diemJ.Visible = false;
            // 
            // diemI
            // 
            this.diemI.AutoSize = true;
            this.diemI.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemI.Location = new System.Drawing.Point(231, 7);
            this.diemI.Name = "diemI";
            this.diemI.Size = new System.Drawing.Size(7, 9);
            this.diemI.TabIndex = 0;
            this.diemI.Text = "I";
            this.diemI.Visible = false;
            // 
            // diemH
            // 
            this.diemH.AutoSize = true;
            this.diemH.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemH.Location = new System.Drawing.Point(211, 7);
            this.diemH.Name = "diemH";
            this.diemH.Size = new System.Drawing.Size(11, 9);
            this.diemH.TabIndex = 0;
            this.diemH.Text = "H";
            this.diemH.Visible = false;
            // 
            // diemG
            // 
            this.diemG.AutoSize = true;
            this.diemG.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemG.Location = new System.Drawing.Point(191, 7);
            this.diemG.Name = "diemG";
            this.diemG.Size = new System.Drawing.Size(11, 9);
            this.diemG.TabIndex = 0;
            this.diemG.Text = "G";
            this.diemG.Visible = false;
            // 
            // diemF
            // 
            this.diemF.AutoSize = true;
            this.diemF.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemF.Location = new System.Drawing.Point(162, 7);
            this.diemF.Name = "diemF";
            this.diemF.Size = new System.Drawing.Size(10, 9);
            this.diemF.TabIndex = 0;
            this.diemF.Text = "F";
            this.diemF.Visible = false;
            // 
            // diemE
            // 
            this.diemE.AutoSize = true;
            this.diemE.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemE.Location = new System.Drawing.Point(142, 7);
            this.diemE.Name = "diemE";
            this.diemE.Size = new System.Drawing.Size(10, 9);
            this.diemE.TabIndex = 0;
            this.diemE.Text = "E";
            this.diemE.Visible = false;
            // 
            // diemD
            // 
            this.diemD.AutoSize = true;
            this.diemD.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemD.Location = new System.Drawing.Point(122, 7);
            this.diemD.Name = "diemD";
            this.diemD.Size = new System.Drawing.Size(11, 9);
            this.diemD.TabIndex = 0;
            this.diemD.Text = "D";
            this.diemD.Visible = false;
            // 
            // diemC
            // 
            this.diemC.AutoSize = true;
            this.diemC.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemC.Location = new System.Drawing.Point(87, 7);
            this.diemC.Name = "diemC";
            this.diemC.Size = new System.Drawing.Size(11, 9);
            this.diemC.TabIndex = 0;
            this.diemC.Text = "C";
            this.diemC.Visible = false;
            // 
            // diemB
            // 
            this.diemB.AutoSize = true;
            this.diemB.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemB.Location = new System.Drawing.Point(54, 7);
            this.diemB.Name = "diemB";
            this.diemB.Size = new System.Drawing.Size(10, 9);
            this.diemB.TabIndex = 0;
            this.diemB.Text = "B";
            this.diemB.Visible = false;
            // 
            // da
            // 
            this.da.AutoSize = true;
            this.da.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.da.Location = new System.Drawing.Point(142, 29);
            this.da.Name = "da";
            this.da.Size = new System.Drawing.Size(9, 9);
            this.da.TabIndex = 0;
            this.da.Text = "a";
            this.da.Visible = false;
            // 
            // dZ
            // 
            this.dZ.AutoSize = true;
            this.dZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dZ.Location = new System.Drawing.Point(122, 29);
            this.dZ.Name = "dZ";
            this.dZ.Size = new System.Drawing.Size(10, 9);
            this.dZ.TabIndex = 0;
            this.dZ.Text = "Z";
            this.dZ.Visible = false;
            // 
            // dY
            // 
            this.dY.AutoSize = true;
            this.dY.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dY.Location = new System.Drawing.Point(87, 29);
            this.dY.Name = "dY";
            this.dY.Size = new System.Drawing.Size(10, 9);
            this.dY.TabIndex = 0;
            this.dY.Text = "Y";
            this.dY.Visible = false;
            // 
            // dX
            // 
            this.dX.AutoSize = true;
            this.dX.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dX.Location = new System.Drawing.Point(54, 29);
            this.dX.Name = "dX";
            this.dX.Size = new System.Drawing.Size(10, 9);
            this.dX.TabIndex = 0;
            this.dX.Text = "X";
            this.dX.Visible = false;
            // 
            // dW
            // 
            this.dW.AutoSize = true;
            this.dW.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dW.Location = new System.Drawing.Point(15, 29);
            this.dW.Name = "dW";
            this.dW.Size = new System.Drawing.Size(13, 9);
            this.dW.TabIndex = 0;
            this.dW.Text = "W";
            this.dW.Visible = false;
            // 
            // diemA
            // 
            this.diemA.AutoSize = true;
            this.diemA.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.diemA.Location = new System.Drawing.Point(15, 7);
            this.diemA.Name = "diemA";
            this.diemA.Size = new System.Drawing.Size(10, 9);
            this.diemA.TabIndex = 0;
            this.diemA.Text = "A";
            this.diemA.Visible = false;
            // 
            // VeHinhCoBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 691);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "VeHinhCoBan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vẽ hình cơ bản";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.controlVe.ResumeLayout(false);
            this.controlVe.PerformLayout();
            this.toaDoCacDiem.ResumeLayout(false);
            this.toaDoCacDiem.PerformLayout();
            this.heTrucToaDo.ResumeLayout(false);
            this.heTrucToaDo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel controlVe;
        private System.Windows.Forms.Panel toaDoCacDiem;
        private System.Windows.Forms.Panel heTrucToaDo;
        private System.Windows.Forms.Button ve3D;
        private System.Windows.Forms.Button ve2D;
        private System.Windows.Forms.Button hinh2;
        private System.Windows.Forms.Button hinh1;
        private System.Windows.Forms.Button chuyenDong2;
        private System.Windows.Forms.Button chuyenDong1;
        private System.Windows.Forms.Label dV;
        private System.Windows.Forms.Label dU;
        private System.Windows.Forms.Label dT;
        private System.Windows.Forms.Label dS;
        private System.Windows.Forms.Label dR;
        private System.Windows.Forms.Label dQ;
        private System.Windows.Forms.Label diemP;
        private System.Windows.Forms.Label diemO;
        private System.Windows.Forms.Label diemN;
        private System.Windows.Forms.Label diemM;
        private System.Windows.Forms.Label diemL;
        private System.Windows.Forms.Label diemK;
        private System.Windows.Forms.Label diemJ;
        private System.Windows.Forms.Label diemI;
        private System.Windows.Forms.Label diemH;
        private System.Windows.Forms.Label diemG;
        private System.Windows.Forms.Label diemF;
        private System.Windows.Forms.Label diemE;
        private System.Windows.Forms.Label diemD;
        private System.Windows.Forms.Label diemC;
        private System.Windows.Forms.Label diemB;
        private System.Windows.Forms.Label diemA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label d22;
        private System.Windows.Forms.Label d1;
        private System.Windows.Forms.Label d21;
        private System.Windows.Forms.Label d2;
        private System.Windows.Forms.Label d20;
        private System.Windows.Forms.Label d3;
        private System.Windows.Forms.Label d19;
        private System.Windows.Forms.Label d4;
        private System.Windows.Forms.Label d18;
        private System.Windows.Forms.Label d5;
        private System.Windows.Forms.Label d17;
        private System.Windows.Forms.Label d16;
        private System.Windows.Forms.Label d6;
        private System.Windows.Forms.Label d15;
        private System.Windows.Forms.Label d7;
        private System.Windows.Forms.Label d14;
        private System.Windows.Forms.Label d8;
        private System.Windows.Forms.Label d13;
        private System.Windows.Forms.Label d9;
        private System.Windows.Forms.Label d12;
        private System.Windows.Forms.Label d10;
        private System.Windows.Forms.Label d11;
        private System.Windows.Forms.Label d27;
        private System.Windows.Forms.Label d26;
        private System.Windows.Forms.Label d25;
        private System.Windows.Forms.Label d24;
        private System.Windows.Forms.Label d23;
        private System.Windows.Forms.Label da;
        private System.Windows.Forms.Label dZ;
        private System.Windows.Forms.Label dY;
        private System.Windows.Forms.Label dX;
        private System.Windows.Forms.Label dW;
        private System.Windows.Forms.Label t1;
        private System.Windows.Forms.Label t2;
        private System.Windows.Forms.Label t3;
        private System.Windows.Forms.Label t4;
        private System.Windows.Forms.Label t5;
        private System.Windows.Forms.Label t6;
        private System.Windows.Forms.Label t7;
        private System.Windows.Forms.Label t8;
        private System.Windows.Forms.Label t9;
        private System.Windows.Forms.Label t10;
        private System.Windows.Forms.Label t11;
        private System.Windows.Forms.Label t12;
        private System.Windows.Forms.Label t13;
        private System.Windows.Forms.Label t31;
        private System.Windows.Forms.Label t30;
        private System.Windows.Forms.Label t29;
        private System.Windows.Forms.Label t28;
        private System.Windows.Forms.Label t27;
        private System.Windows.Forms.Label t26;
        private System.Windows.Forms.Label t25;
        private System.Windows.Forms.Label t24;
        private System.Windows.Forms.Label t23;
        private System.Windows.Forms.Label t22;
        private System.Windows.Forms.Label t21;
        private System.Windows.Forms.Label t20;
        private System.Windows.Forms.Label t19;
        private System.Windows.Forms.Label t18;
        private System.Windows.Forms.Label t17;
        private System.Windows.Forms.Label t16;
        private System.Windows.Forms.Label t15;
        private System.Windows.Forms.Label t14;
        private System.Windows.Forms.Label t33;
        private System.Windows.Forms.Label t32;
        private System.Windows.Forms.Button hinhTru;
        private System.Windows.Forms.Button hinhHCN;
        private System.Windows.Forms.Label t34;
        private System.Windows.Forms.Label t35;
        private System.Windows.Forms.Label t36;
        private System.Windows.Forms.Label t37;
        private System.Windows.Forms.Label t38;
        private System.Windows.Forms.Label t39;
        private System.Windows.Forms.Label t40;
        private System.Windows.Forms.Label t41;
        private System.Windows.Forms.Label t42;
        private System.Windows.Forms.Label t43;
        private System.Windows.Forms.Label t46;
        private System.Windows.Forms.Label t45;
        private System.Windows.Forms.Label t44;
        private System.Windows.Forms.TextBox hcn3;
        private System.Windows.Forms.TextBox hcn4;
        private System.Windows.Forms.TextBox hcn2;
        private System.Windows.Forms.Button hcn1;
        private System.Windows.Forms.TextBox hcn7;
        private System.Windows.Forms.TextBox hcn8;
        private System.Windows.Forms.TextBox hcn6;
        private System.Windows.Forms.Button hcn5;
        private System.Windows.Forms.Button hcn9;
        private System.Windows.Forms.Button hcn10;
        private System.Windows.Forms.Label labelh7;
        private System.Windows.Forms.Label labelh6;
        private System.Windows.Forms.Label labelh5;
        private System.Windows.Forms.Label labelh4;
        private System.Windows.Forms.Label labelh3;
        private System.Windows.Forms.Label labelh1;
        private System.Windows.Forms.Label labelh2;
        private System.Windows.Forms.Button tyLeCD;
        private System.Windows.Forms.Button doiXungCD;
        private System.Windows.Forms.Button quayCD;
        private System.Windows.Forms.Button tinhTienCD;
        private System.Windows.Forms.Button tinhTienCon1;
        private System.Windows.Forms.Button thucHienBienDoi;
        private System.Windows.Forms.Button gocQuayCD;
        private System.Windows.Forms.TextBox y1cd;
        private System.Windows.Forms.TextBox x1cd;
        private System.Windows.Forms.TextBox goc1CD;
        private System.Windows.Forms.TextBox y2cd;
        private System.Windows.Forms.TextBox x2cd;
        private System.Windows.Forms.Button tinhTienCon2;
    }
}

