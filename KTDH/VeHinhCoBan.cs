﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KTDH
{
    public partial class VeHinhCoBan : Form
    {
        public VeHinhCoBan()
        {
            InitializeComponent();
        }

        int trucToaDo2D = 0; 
        bool veHinh1 = false, veHinh2 = false, chDong1 = false, chDong2 = false, veHHCN = false, anhien = false, veHTT = false;
        int biendoiCD = 0, phepbiendoi = 0; bool bienDoiPaint = false;
        public bool IsNumber(string pText)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }

        void Truc2D(PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.Black, 10, 350, 1000, 350); //trục Ox
            e.Graphics.DrawLine(Pens.Black, 995, 345, 1000, 350);
            e.Graphics.DrawLine(Pens.Black, 995, 355, 1000, 350);

            e.Graphics.DrawLine(Pens.Black, 500, 1, 500, 680); //trục Oy
            e.Graphics.DrawLine(Pens.Black, 495, 6, 500, 1);
            e.Graphics.DrawLine(Pens.Black, 505, 6, 500, 1);
            for (int i = 0; i < 980; i += 10)   //chia vạch
                e.Graphics.DrawLine(Pens.Black, 20 + i, 347, 20 + i, 353);

            for (int i = 0; i < 670; i += 10)
                e.Graphics.DrawLine(Pens.Black, 497, 10 + i, 503, 10 + i);
        }
        void XoaTruc2D(PaintEventArgs e)
        {
            e.Graphics.DrawLine(Pens.White, 10, 350, 1000, 350); //trục Ox
            e.Graphics.DrawLine(Pens.White, 995, 345, 1000, 350);
            e.Graphics.DrawLine(Pens.White, 995, 355, 1000, 350);

            e.Graphics.DrawLine(Pens.White, 500, 1, 500, 680); //trục Oy
            e.Graphics.DrawLine(Pens.White, 495, 6, 500, 1);
            e.Graphics.DrawLine(Pens.White, 505, 6, 500, 1);
            for (int i = 0; i < 980; i += 10)   //chia vạch
                e.Graphics.DrawLine(Pens.White, 20 + i, 347, 20 + i, 353);

            for (int i = 0; i < 670; i += 10)
                e.Graphics.DrawLine(Pens.White, 497, 10 + i, 503, 10 + i);
        }

        void ToaDoDiem(bool a, bool b)
        {
            diemA.Visible = diemB.Visible = diemC.Visible = diemD.Visible = diemE.Visible = diemF.Visible = diemG.Visible
                = diemH.Visible = diemI.Visible = diemJ.Visible = diemK.Visible = diemL.Visible = diemM.Visible
                = diemN.Visible = diemO.Visible = diemP.Visible = dQ.Visible = dR.Visible = dS.Visible = dT.Visible
                = dU.Visible = a;
            d1.Visible = d2.Visible = d3.Visible = d4.Visible = d5.Visible = d6.Visible = d7.Visible = d8.Visible =
                d9.Visible = d10.Visible = d11.Visible = d12.Visible = d13.Visible = d14.Visible = d15.Visible =
                d16.Visible = d17.Visible = d18.Visible = d19.Visible = d20.Visible = d21.Visible = b;
            // = d22.Visible = d23.Visible = d24.Visible = d25.Visible = d26.Visible = d27.Visible
            // dV.Visible = dW.Visible = dX.Visible = dY.Visible = dZ.Visible = da.Visible =
        }

        void ChiaDoDo2D(bool b)
        {
            t1.Visible = t2.Visible = t3.Visible = t4.Visible = t5.Visible = t6.Visible = t7.Visible = t8.Visible =
                t9.Visible = t10.Visible = t11.Visible = t12.Visible = t13.Visible = t14.Visible = t15.Visible = t16.Visible
                = t17.Visible = t18.Visible = t19.Visible = t20.Visible = t21.Visible = t22.Visible = t23.Visible =
                t24.Visible = t25.Visible = t26.Visible = t27.Visible = t28.Visible = t29.Visible = t30.Visible = t31.Visible
                 = t32.Visible = t33.Visible = b;
        }
        void ChiaDoDo3D(bool b)
        {
            t1.Visible = t2.Visible = t3.Visible = t4.Visible = t5.Visible = t6.Visible = t7.Visible = t8.Visible =
                t9.Visible = t10.Visible = t11.Visible = t12.Visible = t13.Visible = t14.Visible = t15.Visible = t16.Visible
                = t17.Visible = t18.Visible = t19.Visible = t20.Visible = t21.Visible = t22.Visible = t23.Visible =
                t24.Visible = t25.Visible = t26.Visible = t27.Visible = t28.Visible = t29.Visible = t30.Visible = t31.Visible
                = t32.Visible = t33.Visible = t34.Visible = t35.Visible = t36.Visible = t37.Visible =
                t38.Visible = t39.Visible = t40.Visible = t40.Visible = t41.Visible = t42.Visible =
                t43.Visible = t44.Visible = t45.Visible = t46.Visible = b;
        }

        private void Ve2D_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            chuyenDong1.Visible = chuyenDong2.Visible = tinhTienCon1.Visible = thucHienBienDoi.Visible
                = x1cd.Visible = y1cd.Visible = gocQuayCD.Visible = goc1CD.Visible = tinhTienCon2.Visible
                = x2cd.Visible = y2cd.Visible = chDong1 = chDong2 = veHinh1 = veHinh2 = false;
            biendoiCD = phepbiendoi = 0;
            tinhTienCD.Visible = quayCD.Visible = doiXungCD.Visible = tyLeCD.Visible = false;
            labelh1.Visible = labelh2.Visible = labelh3.Visible = labelh4.Visible = labelh5.Visible 
                = labelh6.Visible = labelh7.Visible = false;
            hcn1.Visible = hcn2.Visible = hcn3.Visible = hcn4.Visible = hcn5.Visible =
                hcn6.Visible = hcn7.Visible = hcn8.Visible = hcn9.Visible = hcn10.Visible = false;
            hinhHCN.Visible = hinhTru.Visible = false;
            
            hinh1.Visible = hinh2.Visible  = true;
            trucToaDo2D = 1;
            ToaDoDiem(false, false); ChiaDoDo3D(false); ChiaDoDo2D(true); anhien = true;
            heTrucToaDo.Visible = true;
            this.Refresh();
        }
        private void Ve3D_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            chuyenDong1.Visible = chuyenDong2.Visible = tinhTienCon1.Visible = thucHienBienDoi.Visible
                = x1cd.Visible = y1cd.Visible = gocQuayCD.Visible = goc1CD.Visible = tinhTienCon2.Visible
                = x2cd.Visible = y2cd.Visible = false;
            biendoiCD = phepbiendoi = 0;
            tinhTienCD.Visible = quayCD.Visible = doiXungCD.Visible = tyLeCD.Visible = false;
            hcn1.Visible = hcn2.Visible = hcn3.Visible = hcn4.Visible = hcn5.Visible =
                hcn6.Visible = hcn7.Visible = hcn8.Visible = hcn9.Visible = labelh1.Visible = labelh2.Visible
                = labelh3.Visible = labelh4.Visible = labelh5.Visible = labelh6.Visible = labelh7.Visible = false;
            hinhHCN.Visible = hinhTru.Visible = true;
            veHinh2 = chDong1 = chDong2 = veHinh1 = false;
            hinh1.Visible = hinh2.Visible = chuyenDong1.Visible = chuyenDong2.Visible = false;
            trucToaDo2D = 2;
            
            ToaDoDiem(false,false); ChiaDoDo2D(false);

            anhien = true; ChiaDoDo3D(true);
            heTrucToaDo.Visible = true;
            this.Refresh();
        }

        private void Hinh1_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            chuyenDong1.Visible = chuyenDong2.Visible = tinhTienCon1.Visible = thucHienBienDoi.Visible
                = x1cd.Visible = y1cd.Visible = gocQuayCD.Visible = goc1CD.Visible = tinhTienCon2.Visible
                = x2cd.Visible = y2cd.Visible = false;
            biendoiCD = 1; phepbiendoi = 0;
            tinhTienCD.Visible = quayCD.Visible = doiXungCD.Visible = tyLeCD.Visible = true;
            labelh1.Visible = labelh2.Visible
                = labelh3.Visible = labelh4.Visible = labelh5.Visible  =  true;
            labelh6.Visible = labelh7.Visible = false;
            chuyenDong1.Visible = true; chuyenDong2.Visible = false;
            veHinh1 = true; chDong1 = chDong2 = veHinh2 = false;
            ChiaDoDo2D(true); ToaDoDiem(false,false); anhien = true;
            ToaDoDiem(true,true);
            labelh1.Text = "Hình tròn tâm A, bán kính r = 20";
            //labelh2.Text = "Hình tròn tâm H, bán kính r = 20";
            labelh2.Text = "Hình chữ nhật HKJI";
            labelh3.Text = "Hình chữ nhật LMPQ";
            labelh4.Text = "Ngũ giác NRTSO";
            labelh5.Text = "";
            dU.Visible = d21.Visible = false;
            this.Refresh();
        }

        void HienThiToaDo1(Point a, Point b, Point c, Point d, Point e, Point f, Point g, Point h, Point i,
            Point j, Point k, Point l, Point m, Point n, Point o, Point p, Point q, Point r, Point s, Point t)
        {
            //,Point u, Point v, Point w, Point x, Point y, Point z, Point aa           

            diemA.Location = new Point(a.X + 1, a.Y); d1.Text = "A(" + (a.X - 500) / 10 + "," + (350 - a.Y) / 10 + ")";
            diemB.Location = new Point(b.X + 1, b.Y + 5); d2.Text = "B(" + (b.X - 500) / 10 + "," + (350 - b.Y) / 10 + ")";
            diemC.Location = new Point(c.X + 1, c.Y - 7); d3.Text = "C(" + (c.X - 500) / 10 + "," + (350 - c.Y) / 10 + ")";
            diemD.Location = new Point(d.X, d.Y + 1); d4.Text = "D(" + (d.X - 500) / 10 + "," + (350 - d.Y) / 10 + ")";
            diemE.Location = new Point(e.X - 10, e.Y + 1); d5.Text = "E(" + (e.X - 500) / 10 + "," + (350 - e.Y) / 10 + ")";
            diemF.Location = new Point(f.X - 3, f.Y + 5); d6.Text = "F(" + (f.X - 500) / 10 + "," + (350 - f.Y) / 10 + ")";
            diemG.Location = new Point(g.X - 3, g.Y + 5); d7.Text = "G(" + (g.X - 500) / 10 + "," + (350 - g.Y) / 10 + ")";

            diemH.Location = new Point(h.X - 3, h.Y - 10); d8.Text = "H(" + (float)(h.X - 500) / 10 + "," + (float)(350 - h.Y) / 10 + ")";
            diemI.Location = new Point(i.X, i.Y + 2); d9.Text = "I(" + (float)(i.X - 500) / 10 + "," + (float)(350 - i.Y) / 10 + ")";
            diemJ.Location = new Point(j.X - 11, j.Y + 1); d10.Text = "J(" + (float)(j.X - 500) / 10 + "," + (float)(350 - j.Y) / 10 + ")";
            diemK.Location = new Point(k.X - 11, k.Y - 10); d11.Text = "K(" + (float)(k.X - 500) / 10 + "," + (float)(350 - k.Y) / 10 + ")";
            diemL.Location = new Point(l.X - 5, l.Y - 10); d12.Text = "L(" + (float)(l.X - 500) / 10 + "," + (float)(350 - l.Y) / 10 + ")";
            diemM.Location = new Point(m.X, m.Y - 10); d13.Text = "M(" + (float)(m.X - 500) / 10 + "," + (float)(350 - m.Y) / 10 + ")";

            diemN.Location = new Point(n.X + 1, n.Y - 9); d14.Text = "N(" + (float)(n.X - 500) / 10 + "," + (float)(350 - n.Y) / 10 + ")";
            diemO.Location = new Point(o.X + 1, o.Y + 1); d15.Text = "O(" + (float)(o.X - 500) / 10 + "," + (float)(350 - o.Y) / 10 + ")";
            diemP.Location = new Point(p.X, p.Y + 3); d16.Text = "P(" + (float)(p.X - 500) / 10 + "," + (float)(350 - p.Y) / 10 + ")";
            dQ.Location = new Point(q.X - 5, q.Y + 3); d17.Text = "Q(" + (float)(q.X - 500) / 10 + "," + (float)(350 - q.Y) / 10 + ")";
            dR.Location = new Point(r.X, r.Y - 10); d18.Text = "R(" + (float)(r.X - 500) / 10 + "," + (float)(350 - r.Y) / 10 + ")";
            dS.Location = new Point(s.X, s.Y + 1); d19.Text = "S(" + (float)(s.X - 500) / 10 + "," + (float)(350 - s.Y) / 10 + ")";
            dT.Location = new Point(t.X + 1, t.Y); d20.Text = "T(" + (float)(t.X - 500) / 10 + "," + (float)(350 - t.Y) / 10 + ")";
            d21.Text = "";
            //diemH.Location = new Point(700, 231); d8.Text = "H(" + (700 - 500) / 10 + "," + (350 - 230) / 10 + ")";
            //diemI.Location = new Point(701, 255); d9.Text = "I(" + (700 - 500) / 10 + "," + (350 - 250) / 10 + ")";
            //diemJ.Location = new Point(701, 293); d10.Text = "J(" + (700 - 500) / 10 + "," + (350 - 300) / 10 + ")";
            //diemK.Location = new Point(670, 261); d11.Text = "K(" + (670 - 500) / 10 + "," + (350 - 260) / 10 + ")";
            //diemL.Location = new Point(730, 261); d12.Text = "L(" + (730 - 500) / 10 + "," + (350 - 260) / 10 + ")";
            //diemM.Location = new Point(677, 355); d13.Text = "M(" + (680 - 500) / 10 + "," + (350 - 350) / 10 + ")";
            //diemN.Location = new Point(717, 355); d14.Text = "N(" + (720 - 500) / 10 + "," + (350 - 350) / 10 + ")";
        }

        private void Hinh2_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            chuyenDong1.Visible = chuyenDong2.Visible = tinhTienCon1.Visible = thucHienBienDoi.Visible 
                = x1cd.Visible = y1cd.Visible = gocQuayCD.Visible = goc1CD.Visible = tinhTienCon2.Visible 
                = x2cd.Visible = y2cd.Visible = false;
            biendoiCD = 2; phepbiendoi = 0;
            tinhTienCD.Visible = quayCD.Visible = doiXungCD.Visible = tyLeCD.Visible = true;
            chuyenDong2.Visible = true; anhien = true;
            chuyenDong1.Visible =false; veHinh1 = false; 
            veHinh2 = true; chDong1 = chDong2 = veHinh1 = false;            ToaDoDiem(false,false); ChiaDoDo2D(true);
            labelh1.Visible = labelh2.Visible
                = labelh3.Visible = labelh4.Visible = labelh5.Visible = labelh6.Visible = labelh7.Visible = true;

            labelh1.Text = "Hình tròn tâm L, bán kính r = 3";
            labelh2.Text = "Hình tròn tâm L, bán kính r = 2.5";
            labelh3.Text = "Hình tròn tâm G, bán kính r = 3";
            labelh4.Text = "Hình tròn tâm G, bán kính r = 2.5";
            labelh5.Text = "Hình tròn tâm A, bán kính r = " + 0.8;
            labelh6.Text = "Hình tam giác BLQ";
            labelh7.Text = "Tứ giác BCTQ";
                            
            ToaDoDiem(true,true);
            this.Refresh();
        }

        void HienThiToaDo2(Point a, Point b, Point c, Point d, Point e, Point f, Point g, Point h, Point i,
            Point j, Point k, Point l, Point m, Point n, Point o, Point p, Point q, Point r, Point s, Point t, Point u)
        {
            diemA.Location = new Point(a.X-3, a.Y-15); d1.Text = "A(" + (float)(a.X - 500) / 10 + "," + (float)(350 - a.Y) / 10 + ")";
            diemB.Location = new Point(b.X-3, b.Y+8); d2.Text = "B(" + (float)(b.X - 500) / 10 + "," + (float)(350 - b.Y) / 10 + ")";
            diemC.Location = new Point(c.X+2, c.Y-5); d3.Text = "C(" + (float)(c.X - 500) / 10 + "," + (float)(350 - c.Y) / 10 + ")";
            diemD.Location = new Point(d.X, d.Y-10); d4.Text = "D(" + (float)(d.X - 500) / 10 + "," + (float)(350 - d.Y) / 10 + ")";
            diemE.Location = new Point(e.X, e.Y-10); d5.Text = "E(" + (float)(e.X - 500) / 10 + "," + (float)(350 - e.Y) / 10 + ")";
            diemF.Location = new Point(f.X, f.Y-10); d6.Text = "F(" + (float)(f.X - 500) / 10 + "," + (float)(350 - f.Y) / 10 + ")";
            diemG.Location = new Point(g.X+1, g.Y-10); d7.Text = "G(" + (float)(g.X - 500) / 10 + "," + (float)(350 - g.Y) / 10 + ")";
            diemH.Location = new Point(h.X+1, h.Y+3); d8.Text = "H(" + (float)(h.X - 500) / 10 + "," + (float)(350 - h.Y) / 10 + ")";
            diemI.Location = new Point(i.X -9, i.Y+1); d9.Text = "I(" + (float)(i.X - 500) / 10 + "," + (float)(350 - i.Y) / 10 + ")";
            diemJ.Location = new Point(j.X+1, j.Y-11); d10.Text = "J(" + (float)(j.X - 500) / 10 + "," + (float)(350 - j.Y) / 10 + ")";
            diemK.Location = new Point(k.X+3, k.Y+1); d11.Text = "K(" + (float)(k.X - 500) / 10 + "," + (float)(350 - k.Y) / 10 + ")";
            diemL.Location = new Point(l.X-9, l.Y+1); d12.Text = "L(" + (float)(l.X - 500) / 10 + "," + (float)(350 - l.Y) / 10 + ")";
            diemM.Location = new Point(m.X+1, m.Y-12); d13.Text = "M(" + (float)(m.X - 500) / 10 + "," + (float)(350 - m.Y) / 10 + ")";
            diemN.Location = new Point(n.X -11, n.Y+5); d14.Text = "N(" + (float)(n.X - 500) / 10 + "," + (float)(350 - n.Y) / 10 + ")";
            diemO.Location = new Point(o.X+4, o.Y-10); d15.Text = "O(" + (float)(o.X - 500) / 10 + "," + (float)(350 - o.Y) / 10 + ")";
            diemP.Location = new Point(p.X-12, p.Y+1); d16.Text = "P(" + (float)(p.X - 500) / 10 + "," + (float)(350 - p.Y) / 10 + ")";
            dQ.Location = new Point(q.X+1, q.Y); d17.Text = "Q(" + (float)(q.X - 500) / 10 + "," + (float)(350 - q.Y) / 10 + ")";
            dR.Location = new Point(r.X+1, r.Y-7); d18.Text = "R(" + (float)(r.X - 500) / 10 + "," + (float)(350 - r.Y) / 10 + ")";
            dS.Location = new Point(s.X, s.Y+1); d19.Text = "S(" + (float)(s.X - 500) / 10 + "," + (float)(350 - s.Y) / 10 + ")";
            dT.Location = new Point(t.X+2, t.Y); d20.Text = "T(" + (float)(t.X - 500) / 10 + "," + (float)(350 - t.Y) / 10 + ")";
            dU.Location = new Point(u.X-1, u.Y); d21.Text = "U(" + (float)(u.X - 500) / 10 + "," + (float)(350 - u.Y) / 10 + ")";          

        }

        private void hinhHCN_Click(object sender, EventArgs e)
        {
            diemA.Visible = diemB.Visible = diemC.Visible = diemD.Visible = diemE.Visible = diemF.Visible
                = d1.Visible = d2.Visible = d3.Visible = d4.Visible = d5.Visible = d6.Visible = false;
             hcn10.Visible = false;
            hcn1.Visible = hcn2.Visible = hcn3.Visible = hcn4.Visible = hcn5.Visible = 
                hcn6.Visible = hcn7.Visible = hcn8.Visible = hcn9.Visible = true;
            labelh1.Visible = labelh2.Visible = labelh3.Visible = labelh4.Visible = labelh5.Visible
                = labelh6.Visible = labelh7.Visible = false;
            hcn1.Text = "Tọa độ điểm gốc(x,y,z):";
            hcn5.Text = "Chiều dài, cao, rộng:";
            veHTT = false; this.Refresh();
        }

        private void hinhTru_Click(object sender, EventArgs e)
        {
            diemA.Visible = diemB.Visible = diemC.Visible = diemD.Visible = diemE.Visible = diemF.Visible
                = diemG.Visible = diemH.Visible = d1.Visible = d2.Visible = d3.Visible = d4.Visible
                = d5.Visible = d6.Visible = d7.Visible = d8.Visible = false;
            veHHCN = false; hcn7.Visible = hcn9.Visible = false;
            hcn1.Visible = hcn2.Visible = hcn3.Visible = hcn4.Visible = hcn5.Visible =
                hcn6.Visible = hcn8.Visible = hcn10.Visible =  true;
            labelh1.Visible = labelh2.Visible = labelh3.Visible = labelh4.Visible = labelh5.Visible
                = labelh6.Visible = labelh7.Visible = false;
            hcn1.Text = "Tọa độ tâm(x,y,z):";
            hcn5.Text = "Bán kính, Chiều cao:";
            this.Refresh();

        }

        private void toaDoCacDiem_Paint(object sender, PaintEventArgs e)
        {

        }
        
        private void TinhTienCD_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            chuyenDong1.Visible = chuyenDong2.Visible = false;
            tinhTienCon1.Visible = thucHienBienDoi.Visible = x1cd.Visible = y1cd.Visible = true;
            tinhTienCon1.Text = "Khoảng cách x, y:";
            gocQuayCD.Visible = goc1CD.Visible = false;
            tinhTienCon2.Visible = x2cd.Visible = y2cd.Visible = false;
            phepbiendoi = 1;

        }

        private void Quay_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            chuyenDong1.Visible = chuyenDong2.Visible = false;
            tinhTienCon1.Visible = thucHienBienDoi.Visible = x1cd.Visible = y1cd.Visible = true;
            tinhTienCon1.Text = "Tâm quay(x,y):";
            gocQuayCD.Visible = goc1CD.Visible = true;
            tinhTienCon2.Visible = x2cd.Visible = y2cd.Visible = false;
            phepbiendoi = 2;
        }

        private void DoiXung_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false;
            tinhTienCon1.Visible = thucHienBienDoi.Visible = x1cd.Visible = y1cd.Visible = true;
            tinhTienCon1.Text = "Điểm 1 (x,y):";
            tinhTienCon2.Visible = x2cd.Visible = y2cd.Visible = true;
            tinhTienCon2.Text = "Điểm 2 (x,y):";
            gocQuayCD.Visible = goc1CD.Visible = false;
            phepbiendoi = 3;
        }

        private void TyLe_Click(object sender, EventArgs e)
        {
            bienDoiPaint = false; chuyenDong1.Visible = chuyenDong2.Visible = false;
            tinhTienCon1.Visible = thucHienBienDoi.Visible = x1cd.Visible = y1cd.Visible = true;
            tinhTienCon1.Text = "Hệ số tỷ lệ x, y:";
            tinhTienCon2.Visible = x2cd.Visible = y2cd.Visible = false;
            gocQuayCD.Visible = goc1CD.Visible = false;
            phepbiendoi = 4;
        }

        private void ThucHienBienDoi_Click(object sender, EventArgs e)
        {
            if(phepbiendoi == 1)
            {
                if (x1cd.Text.Length == 0 || y1cd.Text.Length == 0 || goc1CD.Text.Length == 0)
                {
                    MessageBox.Show("Bạn chưa nhập đầy đủ các thông tin để thực hiện tịnh tiến!");
                }
                else if (IsNumber(x1cd.Text) == false || IsNumber(y1cd.Text) == false || IsNumber(goc1CD.Text) == false)
                {
                    MessageBox.Show("Bạn đã nhập sai định dạng!");
                }
                else
                {
                    bienDoiPaint = true; this.Refresh();
                }
            }
            else if (phepbiendoi == 2)
            {
                if (x1cd.Text.Length == 0 || y1cd.Text.Length == 0)
                {
                    MessageBox.Show("Bạn chưa nhập đầy đủ các thông tin để thực hiện quay!");
                }
                else if (IsNumber(x1cd.Text) == false || IsNumber(y1cd.Text) == false)
                {
                    MessageBox.Show("Bạn đã nhập sai định dạng!");
                }
                else
                {
                    bienDoiPaint = true; this.Refresh();
                }
            }
            else if(phepbiendoi == 3)
            {
                if (x1cd.Text.Length == 0 || y1cd.Text.Length == 0 || x2cd.Text.Length == 0 || y2cd.Text.Length == 0)
                {
                    MessageBox.Show("Bạn chưa nhập đầy đủ các thông tin để thực hiện đối xứng!");
                }
                else if (IsNumber(x1cd.Text) == false || IsNumber(y1cd.Text) == false
                    || IsNumber(x2cd.Text) == false || IsNumber(y2cd.Text) == false)
                {
                    MessageBox.Show("Bạn đã nhập sai định dạng!");
                }
                else if ((x1cd.Text != "0" || x2cd.Text != "0" || y1cd.Text != "0" || y2cd.Text != "0")
                    && x1cd.Text == x2cd.Text && y1cd.Text == y2cd.Text)
                {
                    MessageBox.Show("Bạn đã nhập sai định dạng! Điểm đầu và điểm cuối phải khác nhau!");
                }
                else
                {
                    bienDoiPaint = true; this.Refresh();
                }
            }
            else if (phepbiendoi == 4)
            {
                if (x1cd.Text.Length == 0 || y1cd.Text.Length == 0)
                {
                    MessageBox.Show("Bạn chưa nhập đầy đủ các thông tin để thực hiện phép tỷ lệ!");
                }
                else if (IsNumber(x1cd.Text) == false || IsNumber(y1cd.Text) == false)
                {
                    MessageBox.Show("Bạn đã nhập sai định dạng!");
                }
                else
                {
                    bienDoiPaint = true; this.Refresh();
                }
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            if (anhien == false)
            {
                anhien = true;
                if (trucToaDo2D == 2)
                {
                    ChiaDoDo2D(false);
                    ChiaDoDo3D(true);
                    ToaDoDiem(false, false);
                    if (veHHCN == true) d7.Visible = d8.Visible = false;
                }
                else
                {
                    ToaDoDiem(true,true);
                    if (veHinh1 == true) d21.Visible = dU.Visible = false;
                    ChiaDoDo3D(false);
                    ChiaDoDo2D(true);
                }
            }
            else if (anhien == true)
            {
                anhien = false;
                if(trucToaDo2D == 2)
                {
                    ToaDoDiem(false,false);
                    ChiaDoDo2D(false);
                    ChiaDoDo3D(false);
                }
                else
                {
                    ToaDoDiem(false,true);
                    ChiaDoDo2D(false);
                    ChiaDoDo3D(false);
                }
            }
        }

        private void hcn9_Click(object sender, EventArgs e)
        {
            if(hcn2.Text.Length == 0 || hcn3.Text.Length == 0 || hcn4.Text.Length == 0 
                || hcn6.Text.Length == 0 || hcn7.Text.Length == 0 || hcn8.Text.Length == 0)
            {
                MessageBox.Show("Bạn chưa nhập đầy đủ các thông tin để vẽ!");
            }
            else if (IsNumber(hcn2.Text) == false || IsNumber(hcn3.Text) == false || IsNumber(hcn4.Text) == false
               || IsNumber(hcn6.Text) == false || IsNumber(hcn7.Text) == false || IsNumber(hcn8.Text) == false)
            {
                MessageBox.Show("Bạn đã nhập sai thông tin!");
            }
            else
            {                
                heTrucToaDo.Visible = false;   veHHCN = true;    heTrucToaDo.Visible = true;
            }
        }

        private void hcn10_Click(object sender, EventArgs e)
        {
            if (hcn2.Text.Length == 0 || hcn3.Text.Length == 0 || hcn4.Text.Length == 0
                || hcn6.Text.Length == 0 || hcn8.Text.Length == 0)
            {
                MessageBox.Show("Bạn chưa nhập đầy đủ các thông tin để vẽ!");
            }
            else if (IsNumber(hcn2.Text) == false || IsNumber(hcn3.Text)==false || IsNumber(hcn4.Text)==false
                || IsNumber(hcn6.Text) == false || IsNumber(hcn8.Text) == false)
            {
                MessageBox.Show("Bạn đã nhập sai thông tin!");
            }
            else
            {
                heTrucToaDo.Visible = false; veHTT = true; heTrucToaDo.Visible = true;
            }
        }
        private void ChuyenDong1_Click(object sender, EventArgs e)
        {
            tinhTienCD.Visible = quayCD.Visible = doiXungCD.Visible = tyLeCD.Visible = false;
            labelh1.Visible = labelh2.Visible
                = labelh3.Visible = labelh4.Visible = labelh5.Visible = labelh6.Visible = labelh7.Visible = false;
            heTrucToaDo.Visible = false; chDong1 = true; heTrucToaDo.Visible = true;
            ToaDoDiem(false,false); ChiaDoDo2D(false);
        }

        private void ChuyenDong2_Click(object sender, EventArgs e)
        {
            tinhTienCD.Visible = quayCD.Visible = doiXungCD.Visible = tyLeCD.Visible = false;
            labelh1.Visible = labelh2.Visible
                = labelh3.Visible = labelh4.Visible = labelh5.Visible = labelh6.Visible = labelh7.Visible = false;
            heTrucToaDo.Visible = false; chDong2 = true; heTrucToaDo.Visible = true;
            ToaDoDiem(false,false); ChiaDoDo2D(false);
        }

        private void HeTrucToaDo_Paint(object sender, PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(1018, 685);
            if (trucToaDo2D == 1)
            {
                Ve2Hinh2D ve2 = new Ve2Hinh2D();
                Truc2D(e);
                if(veHinh1 == true)
                {
                    PhepBienDoi chd = new PhepBienDoi();
                    Point[] k = new Point[14];//@@ Kiếm quay tận 13 điểm. huhu
                    Point[] l = new Point[14];
                    Point[] m = new Point[8];
                    m[1].X = 300; m[1].Y = 230; m[2].X = 300; m[2].Y = 250; m[3].X = 300; m[3].Y = 300;
                    m[4].X = 270; m[4].Y = 260; m[5].X = 330; m[5].Y = 260;m[6].X = 280; m[6].Y = 350;
                    m[7].X = 320; m[7].Y = 350;
                    
                    k[1] = new Point(330, 255); k[2] = new Point(330, 260);
                    k[3] = new Point(350, 260); k[4] = new Point(350, 255);
                    k[5] = new Point(350, 245); k[6] = new Point(355, 245);
                    k[7] = new Point(355, 252); k[8] = new Point(355, 263);
                    k[9] = new Point(355, 270); k[10] = new Point(350, 270);
                    k[11] = new Point(410, 252); k[12] = new Point(410, 263);
                    k[13] = new Point(420, 258);
                    ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                        , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.Red);// người trái
                    ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                        k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                        k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                        k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                    e.Graphics.DrawImage(bmp, 0, 0);
                    if(bienDoiPaint == false)
                        HienThiToaDo1(m[1], m[2], m[3], m[4], m[5], m[6], m[7], k[1], k[2], k[3], k[4], k[5], k[6], k[7],
                        k[8], k[9], k[10], k[11], k[12], k[13]);
                    if (bienDoiPaint == true)
                    {
                        if (phepbiendoi == 1)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text) * 10, hsy = Convert.ToInt32(y1cd.Text) * 10;
                            hsy = -hsy;
                            //ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                            //    , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.White);// người trái
                            //ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                            //    k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                            //    k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                            //    k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            Point[] k1 = new Point[14];
                            m[1] = chd.TinhTien(m[1].X, m[1].Y, hsx, hsy); m[2] = chd.TinhTien(m[2].X, m[2].Y, hsx, hsy);
                            m[3] = chd.TinhTien(m[3].X, m[3].Y, hsx, hsy); m[4] = chd.TinhTien(m[4].X, m[4].Y, hsx, hsy);
                            m[5] = chd.TinhTien(m[5].X, m[5].Y, hsx, hsy); m[6] = chd.TinhTien(m[6].X, m[6].Y, hsx, hsy);
                            m[7] = chd.TinhTien(m[7].X, m[7].Y, hsx, hsy);

                            k[1] = chd.TinhTien(k[1].X, k[1].Y, hsx, hsy); k[2] = chd.TinhTien(k[2].X, k[2].Y, hsx, hsy);
                            k[3] = chd.TinhTien(k[3].X, k[3].Y, hsx, hsy); k[4] = chd.TinhTien(k[4].X, k[4].Y, hsx, hsy);
                            k[5] = chd.TinhTien(k[5].X, k[5].Y, hsx, hsy); k[6] = chd.TinhTien(k[6].X, k[6].Y, hsx, hsy);
                            k[7] = chd.TinhTien(k[7].X, k[7].Y, hsx, hsy); k[8] = chd.TinhTien(k[8].X, k[8].Y, hsx, hsy);
                            k[9] = chd.TinhTien(k[9].X, k[9].Y, hsx, hsy); k[10] = chd.TinhTien(k[10].X, k[10].Y, hsx, hsy);
                            k[11] = chd.TinhTien(k[11].X, k[11].Y, hsx, hsy); k[12] = chd.TinhTien(k[12].X, k[12].Y, hsx, hsy);
                            k[13] = chd.TinhTien(k[13].X, k[13].Y, hsx, hsy);
                            Point[] m1 = new Point[8];
                            ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                                 , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.Red);// người trái
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo1(m[1], m[2], m[3], m[4], m[5], m[6], m[7], k[1], k[2], k[3], k[4], k[5], k[6], k[7],
                                k[8], k[9], k[10], k[11], k[12], k[13]);
                        }
                        else if (phepbiendoi == 2)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text) * 10, hsy = Convert.ToInt32(y1cd.Text) * 10,
                                gCD = Convert.ToInt32(goc1CD.Text);
                            hsy = -hsy;
                            //ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                            //    , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.White);// người trái
                            //ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                            //    k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                            //    k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                            //    k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            m[1] = chd.Quay(m[1].X, m[1].Y, hsx, hsy,gCD); m[2] = chd.Quay(m[2].X, m[2].Y, hsx, hsy, gCD);
                            m[3] = chd.Quay(m[3].X, m[3].Y, hsx, hsy, gCD); m[4] = chd.Quay(m[4].X, m[4].Y, hsx, hsy, gCD);
                            m[5] = chd.Quay(m[5].X, m[5].Y, hsx, hsy, gCD); m[6] = chd.Quay(m[6].X, m[6].Y, hsx, hsy, gCD);
                            m[7] = chd.Quay(m[7].X, m[7].Y, hsx, hsy, gCD);

                            k[1] = chd.Quay(k[1].X, k[1].Y, hsx, hsy, gCD); k[2] = chd.Quay(k[2].X, k[2].Y, hsx, hsy, gCD);
                            k[3] = chd.Quay(k[3].X, k[3].Y, hsx, hsy, gCD); k[4] = chd.Quay(k[4].X, k[4].Y, hsx, hsy, gCD);
                            k[5] = chd.Quay(k[5].X, k[5].Y, hsx, hsy, gCD); k[6] = chd.Quay(k[6].X, k[6].Y, hsx, hsy, gCD);
                            k[7] = chd.Quay(k[7].X, k[7].Y, hsx, hsy, gCD); k[8] = chd.Quay(k[8].X, k[8].Y, hsx, hsy, gCD);
                            k[9] = chd.Quay(k[9].X, k[9].Y, hsx, hsy, gCD); k[10] = chd.Quay(k[10].X, k[10].Y, hsx, hsy, gCD);
                            k[11] = chd.Quay(k[11].X, k[11].Y, hsx, hsy, gCD); k[12] = chd.Quay(k[12].X, k[12].Y, hsx, hsy, gCD);
                            k[13] = chd.Quay(k[13].X, k[13].Y, hsx, hsy, gCD);

                            ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                                 , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.Red);// người trái
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo1(m[1], m[2], m[3], m[4], m[5], m[6], m[7], k[1], k[2], k[3], k[4], k[5], k[6], k[7],
                                k[8], k[9], k[10], k[11], k[12], k[13]);
                        }
                        else if (phepbiendoi == 3)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text), hsy = Convert.ToInt32(y1cd.Text),
                               hsx1 = Convert.ToInt32(x2cd.Text), hsy1 = Convert.ToInt32(y2cd.Text);
                            hsy = -hsy; hsy1 = -hsy1;

                            if (hsx == 0 && hsy == 0)
                            {
                                DuongTron dtr = new DuongTron();
                                dtr.MidpointDuongTron(hsx1*10+500, hsy1*10+350, 2, bmp, Color.Red);
                                e.Graphics.DrawImage(bmp, 0, 0);
                            }

                            int A = hsy1 - hsy, B = hsx - hsx1, C = hsx1 * hsy - hsx * hsy1;
                            if (hsx != 0 || hsy != 0)
                            {
                                DuongThang dt = new DuongThang();
                                PhepBienDoi pbd = new PhepBienDoi();
                                int y1, y2;
                                if (B == 0 && A!=0)
                                {
                                    dt.MidpointLine(hsx * 10 + 500, 1 , hsx * 10 + 500, 658, bmp, Color.Red);
                                }
                                else if(A==0 && B!=0)
                                {
                                    dt.MidpointLine(1 ,hsy * 10 + 350, 1018, hsy * 10 + 350, bmp, Color.Red);
                                }
                                else if(A == 0 && B == 0)
                                {
                                    dt.MidpointLine(500-350,350-350, 500+350, 350+350, bmp, Color.Red);
                                }
                                else
                                {
                                    y1 = (-A * 30 - C) / B;
                                    y2 = (-A * 30 - C) / B;
                                    dt.MidpointLine(-30 * 10 + hsx * 10 + 500, -y1 * 10 + hsy * 10 + 350,
                                        30 * 10 + hsx1 * 10 + 500, y2 * 10 + hsy1 * 10 + 350, bmp, Color.Red);
                                }
                                e.Graphics.DrawImage(bmp, 0, 0);
                            }
                            //ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                            //    , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.White);// người trái
                            //ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                            //    k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                            //    k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                            //    k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            m[1] = chd.DoiXung(m[1].X, m[1].Y, hsx, hsy, hsx1, hsy1); m[2] = chd.DoiXung(m[2].X, m[2].Y, hsx, hsy, hsx1, hsy1);
                            m[3] = chd.DoiXung(m[3].X, m[3].Y, hsx, hsy, hsx1, hsy1); m[4] = chd.DoiXung(m[4].X, m[4].Y, hsx, hsy, hsx1, hsy1);
                            m[5] = chd.DoiXung(m[5].X, m[5].Y, hsx, hsy, hsx1, hsy1); m[6] = chd.DoiXung(m[6].X, m[6].Y, hsx, hsy, hsx1, hsy1);
                            m[7] = chd.DoiXung(m[7].X, m[7].Y, hsx, hsy, hsx1, hsy1);

                            k[1] = chd.DoiXung(k[1].X, k[1].Y, hsx, hsy, hsx1, hsy1); k[2] = chd.DoiXung(k[2].X, k[2].Y, hsx, hsy, hsx1, hsy1);
                            k[3] = chd.DoiXung(k[3].X, k[3].Y, hsx, hsy, hsx1, hsy1); k[4] = chd.DoiXung(k[4].X, k[4].Y, hsx, hsy, hsx1, hsy1);
                            k[5] = chd.DoiXung(k[5].X, k[5].Y, hsx, hsy, hsx1, hsy1); k[6] = chd.DoiXung(k[6].X, k[6].Y, hsx, hsy, hsx1, hsy1);
                            k[7] = chd.DoiXung(k[7].X, k[7].Y, hsx, hsy, hsx1, hsy1); k[8] = chd.DoiXung(k[8].X, k[8].Y, hsx, hsy, hsx1, hsy1);
                            k[9] = chd.DoiXung(k[9].X, k[9].Y, hsx, hsy, hsx1, hsy1); k[10] = chd.DoiXung(k[10].X, k[10].Y, hsx, hsy, hsx1, hsy1);
                            k[11] = chd.DoiXung(k[11].X, k[11].Y, hsx, hsy, hsx1, hsy1); k[12] = chd.DoiXung(k[12].X, k[12].Y, hsx, hsy, hsx1, hsy1);
                            k[13] = chd.DoiXung(k[13].X, k[13].Y, hsx, hsy, hsx1, hsy1);

                            ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                                 , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.Red);// người trái
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo1(m[1], m[2], m[3], m[4], m[5], m[6], m[7], k[1], k[2], k[3], k[4], k[5], k[6], k[7],
                                k[8], k[9], k[10], k[11], k[12], k[13]);
                        }
                        else if (phepbiendoi == 4)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text), hsy = Convert.ToInt32(y1cd.Text),
                               hsx1 = m[7].X, hsy1 = 350;
                            int bkdx = 20, bkdy = 20; // bán kính của đầu, nếu hệ số tỉ lệ x,y khác nhau thì sẽ vẽ elip
                            if(hsx>=0)
                            {
                                bkdx *= hsx; 
                            }
                            else
                            {
                                bkdx /= (-hsx);
                            }
                            if(hsy>=0)
                            {
                                bkdy *= hsy;
                            }
                            else
                            {
                                bkdy /= (-hsy);
                            }
                            hsy = -hsy;
                            
                            //ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                            //    , m[6].X, m[6].Y, m[7].X, m[7].Y, 20, 20, bmp, Color.White);// người trái
                            //ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                            //    k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                            //    k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                            //    k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            m[1] = chd.TyLe(m[1].X, m[1].Y, hsx, hsy, hsx1, hsy1); m[2] = chd.TyLe(m[2].X, m[2].Y, hsx, hsy, hsx1, hsy1);
                            m[3] = chd.TyLe(m[3].X, m[3].Y, hsx, hsy, hsx1, hsy1); m[4] = chd.TyLe(m[4].X, m[4].Y, hsx, hsy, hsx1, hsy1);
                            m[5] = chd.TyLe(m[5].X, m[5].Y, hsx, hsy, hsx1, hsy1); m[6] = chd.TyLe(m[6].X, m[6].Y, hsx, hsy, hsx1, hsy1);
                            m[7] = chd.TyLe(m[7].X, m[7].Y, hsx, hsy, hsx1, hsy1);

                            k[1] = chd.TyLe(k[1].X, k[1].Y, hsx, hsy, hsx1, hsy1); k[2] = chd.TyLe(k[2].X, k[2].Y, hsx, hsy, hsx1, hsy1);
                            k[3] = chd.TyLe(k[3].X, k[3].Y, hsx, hsy, hsx1, hsy1); k[4] = chd.TyLe(k[4].X, k[4].Y, hsx, hsy, hsx1, hsy1);
                            k[5] = chd.TyLe(k[5].X, k[5].Y, hsx, hsy, hsx1, hsy1); k[6] = chd.TyLe(k[6].X, k[6].Y, hsx, hsy, hsx1, hsy1);
                            k[7] = chd.TyLe(k[7].X, k[7].Y, hsx, hsy, hsx1, hsy1); k[8] = chd.TyLe(k[8].X, k[8].Y, hsx, hsy, hsx1, hsy1);
                            k[9] = chd.TyLe(k[9].X, k[9].Y, hsx, hsy, hsx1, hsy1); k[10] = chd.TyLe(k[10].X, k[10].Y, hsx, hsy, hsx1, hsy1);
                            k[11] = chd.TyLe(k[11].X, k[11].Y, hsx, hsy, hsx1, hsy1); k[12] = chd.TyLe(k[12].X, k[12].Y, hsx, hsy, hsx1, hsy1);
                            k[13] = chd.TyLe(k[13].X, k[13].Y, hsx, hsy, hsx1, hsy1);

                            ve2.VeHinhNguoi(m[1].X, m[1].Y, m[2].X, m[2].Y, m[3].X, m[3].Y, m[4].X, m[4].Y, m[5].X, m[5].Y
                                 , m[6].X, m[6].Y, m[7].X, m[7].Y, bkdx,bkdy, bmp, Color.Red);// người trái
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo1(m[1], m[2], m[3], m[4], m[5], m[6], m[7], k[1], k[2], k[3], k[4], k[5], k[6], k[7],
                                k[8], k[9], k[10], k[11], k[12], k[13]);
                            if (bkdx == bkdy)
                            {
                                labelh1.Text = "Hình tròn tâm A, bán kính r = " + bkdx;
                                labelh2.Text = "Hình chữ nhật HKJI";
                                labelh3.Text = "Hình chữ nhật LMPQ";
                                labelh4.Text = "Ngũ giác NRTSO";
                            }
                            else
                            {
                                labelh1.Text = "Hình Elip A, bán kính R = " + bkdx+", r = "+bkdy;
                                labelh2.Text = "Hình chữ nhật HKJI";
                                labelh3.Text = "Hình chữ nhật LMPQ";
                                labelh4.Text = "Ngũ giác NRTSO";
                            }
                        }
                    }
                    if (chDong1 == true)
                    {
                        XoaTruc2D(e);
                        DuongThang dth = new DuongThang();
                        dth.MidpointLine(40, 350, 960, 350, bmp, Color.Chocolate);
                        Point [] b = new Point[8]; //Người quay
                        Point[] c = new Point[8];
                        ve2.VeHinhNguoi(700, 230, 700, 250, 700, 300, 670, 260, 730, 260, 680, 350, 720, 350, 20, 20, bmp, Color.Blue);
                        // Vẽ thêm người phải

                        for (int i = 0; i<=15; i++) // kiếm tịnh tiến
                        {
                            for (int j = 1; j < 14; j++) l[j] = k[j];
                            int kc = i * 20; //kcttien theo x
                            k[1] = chd.TinhTien(330, 255, kc, 0); k[2] = chd.TinhTien(330, 260, kc, 0);
                            k[3] = chd.TinhTien(350, 260, kc, 0); k[4] = chd.TinhTien(350, 255, kc, 0);
                            k[5] = chd.TinhTien(350, 245, kc, 0); k[6] = chd.TinhTien(355, 245, kc, 0);
                            k[7] = chd.TinhTien(355, 252, kc, 0); k[8] = chd.TinhTien(355, 263, kc, 0);
                            k[9] = chd.TinhTien(355, 270, kc, 0); k[10] = chd.TinhTien(350, 270, kc, 0);
                            k[11] = chd.TinhTien(410, 252, kc, 0); k[12] = chd.TinhTien(410, 263, kc, 0);
                            k[13] = chd.TinhTien(420, 258, kc, 0);
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y, 
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y, 
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            ve2.VeHinhKiem(l[1].X, l[1].Y, l[2].X, l[2].Y, l[3].X, l[3].Y, l[4].X, l[4].Y,
                                l[5].X, l[5].Y, l[6].X, l[6].Y, l[7].X, l[7].Y,
                                l[8].X, l[8].Y, l[9].X, l[9].Y, l[10].X, l[10].Y,
                                l[11].X, l[11].Y, l[12].X, l[12].Y, l[13].X, l[13].Y, bmp, Color.White);
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                        }
                        Thread.Sleep(1000);
                        for(int g = 90; g<= 90; g+=90)// người và kiếm quay 90 độ
                        {
                            ve2.VeHinhNguoi(700, 230, 700, 250, 700, 300, 670, 260, 730, 260, 680, 350, 720, 350, 20, 20, bmp, Color.White);
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.White);
                            Point tq = new Point(220,0);
                            b[1] = chd.Quay(700, 230, tq.X, tq.Y, g); b[2] = chd.Quay(700, 250, tq.X, tq.Y, g);
                            b[3] = chd.Quay(700, 300, tq.X, tq.Y, g); b[4] = chd.Quay(670, 260, tq.X, tq.Y, g);
                            b[5] = chd.Quay(730, 260, tq.X, tq.Y, g); b[6] = chd.Quay(680, 350, tq.X, tq.Y, g);
                            b[7] = chd.Quay(720, 350, tq.X, tq.Y, g);

                            k[1] = chd.Quay(k[1].X, k[1].Y, tq.X, tq.Y, g); k[2] = chd.Quay(k[2].X, k[2].Y, tq.X, tq.Y, g);
                            k[3] = chd.Quay(k[3].X, k[3].Y, tq.X, tq.Y, g); k[4] = chd.Quay(k[4].X, k[4].Y, tq.X, tq.Y, g);
                            k[5] = chd.Quay(k[5].X, k[5].Y, tq.X, tq.Y, g); k[6] = chd.Quay(k[6].X, k[6].Y, tq.X, tq.Y, g);
                            k[7] = chd.Quay(k[7].X, k[7].Y, tq.X, tq.Y, g); k[8] = chd.Quay(k[8].X, k[8].Y, tq.X, tq.Y, g);
                            k[9] = chd.Quay(k[9].X, k[9].Y, tq.X, tq.Y, g); k[10] = chd.Quay(k[10].X, k[10].Y, tq.X, tq.Y, g);
                            k[11] = chd.Quay(k[11].X, k[11].Y, tq.X, tq.Y, g); k[12] = chd.Quay(k[12].X, k[12].Y, tq.X, tq.Y, g);
                            k[13] = chd.Quay(k[13].X, k[13].Y, tq.X, tq.Y, g);



                            ve2.VeHinhNguoi(b[1].X, b[1].Y, b[2].X, b[2].Y, b[3].X, b[3].Y, b[4].X, b[4].Y, b[5].X, b[5].Y,
                                b[6].X, b[6].Y, b[7].X, b[7].Y, 20, 20, bmp, Color.Blue);// người phải
                            ve2.VeHinhKiem(k[1].X, k[1].Y, k[2].X, k[2].Y, k[3].X, k[3].Y, k[4].X, k[4].Y,
                                k[5].X, k[5].Y, k[6].X, k[6].Y, k[7].X, k[7].Y,
                                k[8].X, k[8].Y, k[9].X, k[9].Y, k[10].X, k[10].Y,
                                k[11].X, k[11].Y, k[12].X, k[12].Y, k[13].X, k[13].Y, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                        }
                        b = new Point[8];
                        for (int i = 0; i<=3; i++)
                        {
                            for(int j=0; j<=5; j++)
                            {
                                for (int n = 1; n < 8; n++) c[n] = b[n];
                                int kc = -j*10;
                                b[1] = chd.TinhTien(300, 230, 0, kc); b[2] = chd.TinhTien(300, 250, 0, kc);
                                b[3] = chd.TinhTien(300, 300, 0, kc); b[4] = chd.TinhTien(270, 260, 0, kc);
                                b[5] = chd.TinhTien(330, 260, 0, kc); b[6] = chd.TinhTien(280, 350, 0, kc);
                                b[7] = chd.TinhTien(320, 350, 0, kc);
                                ve2.VeHinhNguoi(b[1].X, b[1].Y, b[2].X, b[2].Y, b[3].X, b[3].Y, b[4].X, b[4].Y, b[5].X, b[5].Y,
                                    b[6].X, b[6].Y, b[7].X, b[7].Y, 20, 20, bmp, Color.Red);// người phải
                                ve2.VeHinhNguoi(c[1].X, c[1].Y, c[2].X, c[2].Y, c[3].X, c[3].Y, c[4].X, c[4].Y, c[5].X, c[5].Y,
                                c[6].X, c[6].Y, c[7].X, c[7].Y, 20, 20, bmp, Color.White);// người phải
                                ve2.VeHinhNguoi(b[1].X, b[1].Y, b[2].X, b[2].Y, b[3].X, b[3].Y, b[4].X, b[4].Y, b[5].X, b[5].Y,
                                    b[6].X, b[6].Y, b[7].X, b[7].Y, 20, 20, bmp, Color.Red);// người phải
                                e.Graphics.DrawImage(bmp, 0, 0);
                            }
                        }

                        ve2.VeHinhNguoi(b[1].X, b[1].Y, b[2].X, b[2].Y, b[3].X, b[3].Y, b[4].X, b[4].Y, b[5].X, b[5].Y,
                            b[6].X, b[6].Y, b[7].X, b[7].Y, 20, 20, bmp, Color.White);// người phải
                        ve2.VeHinhNguoi(300, 230, 300, 250, 300, 300, 270, 260, 330, 260, 280, 350, 320, 350, 20, 20, bmp, Color.Red);// người trái
                        e.Graphics.DrawImage(bmp, 0, 0);
                    }

                }

                else if (veHinh2 == true)
                {
                    Truc2D(e);
                    DuongThang dth = new DuongThang();
                    PhepBienDoi chd = new PhepBienDoi();
                    Point[] x = new Point[22];
                    Point[] x1 = new Point[22];
                    Point[] x2 = new Point[22];
                    int bkl1 = 30, bkl2 = 30, bkn1 = 25, bkn2 = 25, bke1 = 8, bke2 = 8;
                    int bonusx = 200;
                    x[1].X = 75 + bonusx; x[1].Y = 270;      x[2].X = 75 + bonusx; x[2].Y = 280;       x[3].X = 140 + bonusx; x[3].Y = 270;
                    x[4].X = 140 + bonusx; x[4].Y = 250;     x[5].X = 130 + bonusx; x[5].Y = 240;       x[6].X = 150 + bonusx; x[6].Y = 260;
                    x[7].X = 150 + bonusx; x[7].Y = 320;       x[8].X = 150 + bonusx; x[8].Y = 295;         x[9].X = 175 + bonusx; x[9].Y = 320;
                    x[10].X = 150 + bonusx; x[10].Y = 345;      x[11].X = 125 + bonusx; x[11].Y = 320;      x[12].X = 50 + bonusx; x[12].Y = 320;
                    x[13].X = 50 + bonusx; x[13].Y = 345;       x[14].X = 50 + bonusx; x[14].Y = 295;     x[15].X = 25 + bonusx; x[15].Y = 320;
                    x[16].X = 75 + bonusx; x[16].Y = 320;    x[17].X = 100 + bonusx; x[17].Y = 310;    x[18].X = 100 + bonusx; x[18].Y = 300;
                    x[19].X = 100 + bonusx; x[19].Y = 320;    x[20].X = 143 + bonusx; x[20].Y = 280; x[21].X = 30 + bonusx; x[21].Y = 277;
                    
                    ve2.VeHinhXe(x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14],
                        x[15], x[16], x[17], x[18], x[19], x[20], x[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                    e.Graphics.DrawImage(bmp, 0, 0);
                    for (int i = 1; i < 22; i++) x2[i] = x[i];

                    if(bienDoiPaint == false)HienThiToaDo2(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                             x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21]);
                    if (bienDoiPaint==true)
                    {
                        
                        if (phepbiendoi==1)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text)*10, hsy = Convert.ToInt32(y1cd.Text)*10;
                            hsy = -hsy;
                            //ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                            //        x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2,
                            //        bke1, bke2, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            x2[1] = chd.TinhTien(x2[1].X, x2[1].Y, hsx, hsy); x2[2] = chd.TinhTien(x2[2].X, x2[2].Y, hsx, hsy);
                            x2[3] = chd.TinhTien(x2[3].X, x2[3].Y, hsx, hsy); x2[4] = chd.TinhTien(x2[4].X, x2[4].Y, hsx, hsy);
                            x2[5] = chd.TinhTien(x2[5].X, x2[5].Y, hsx, hsy); x2[6] = chd.TinhTien(x2[6].X, x2[6].Y, hsx, hsy);
                            x2[7] = chd.TinhTien(x2[7].X, x2[7].Y, hsx, hsy); x2[8] = chd.TinhTien(x2[8].X, x2[8].Y, hsx, hsy);
                            x2[9] = chd.TinhTien(x2[9].X, x2[9].Y, hsx, hsy); x2[10] = chd.TinhTien(x2[10].X, x2[10].Y, hsx, hsy);
                            x2[11] = chd.TinhTien(x2[11].X, x2[11].Y, hsx, hsy); x2[12] = chd.TinhTien(x2[12].X, x2[12].Y, hsx, hsy);
                            x2[13] = chd.TinhTien(x2[13].X, x2[13].Y, hsx, hsy); x2[14] = chd.TinhTien(x2[14].X, x2[14].Y, hsx, hsy);
                            x2[15] = chd.TinhTien(x2[15].X, x2[15].Y, hsx, hsy); x2[16] = chd.TinhTien(x2[16].X, x2[16].Y, hsx, hsy);
                            x2[17] = chd.TinhTien(x2[17].X, x2[17].Y, hsx, hsy); x2[18] = chd.TinhTien(x2[18].X, x2[18].Y, hsx, hsy);
                            x2[19] = chd.TinhTien(x2[19].X, x2[19].Y, hsx, hsy); x2[20] = chd.TinhTien(x2[20].X, x2[20].Y, hsx, hsy);
                            x2[21] = chd.TinhTien(x2[21].X, x2[21].Y, hsx, hsy);
                            ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                                x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            //Thread.Sleep(100);
                            HienThiToaDo2(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                                    x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21]);
                            
                            
                        }
                        else if (phepbiendoi == 2)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text) * 10 , hsy = Convert.ToInt32(y1cd.Text) * 10,
                                gCD = Convert.ToInt32(goc1CD.Text);
                            hsy = -hsy;
                            //ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                            //    x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2,
                            //    bke1, bke2, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            x2[1] = chd.Quay(x2[1].X, x2[1].Y, hsx, hsy, gCD); x2[2] = chd.Quay(x2[2].X, x2[2].Y, hsx, hsy, gCD);
                            x2[3] = chd.Quay(x2[3].X, x2[3].Y, hsx, hsy, gCD); x2[4] = chd.Quay(x2[4].X, x2[4].Y, hsx, hsy, gCD);
                            x2[5] = chd.Quay(x2[5].X, x2[5].Y, hsx, hsy, gCD); x2[6] = chd.Quay(x2[6].X, x2[6].Y, hsx, hsy, gCD);
                            x2[7] = chd.Quay(x2[7].X, x2[7].Y, hsx, hsy, gCD); x2[8] = chd.Quay(x2[8].X, x2[8].Y, hsx, hsy, gCD);
                            x2[9] = chd.Quay(x2[9].X, x2[9].Y, hsx, hsy, gCD); x2[10] = chd.Quay(x2[10].X, x2[10].Y, hsx, hsy, gCD);
                            x2[11] = chd.Quay(x2[11].X, x2[11].Y, hsx, hsy, gCD); x2[12] = chd.Quay(x2[12].X, x2[12].Y, hsx, hsy, gCD);
                            x2[13] = chd.Quay(x2[13].X, x2[13].Y, hsx, hsy, gCD); x2[14] = chd.Quay(x2[14].X, x2[14].Y, hsx, hsy, gCD);
                            x2[15] = chd.Quay(x2[15].X, x2[15].Y, hsx, hsy, gCD); x2[16] = chd.Quay(x2[16].X, x2[16].Y, hsx, hsy, gCD);
                            x2[17] = chd.Quay(x2[17].X, x2[17].Y, hsx, hsy, gCD); x2[18] = chd.Quay(x2[18].X, x2[18].Y, hsx, hsy, gCD);
                            x2[19] = chd.Quay(x2[19].X, x2[19].Y, hsx, hsy, gCD); x2[20] = chd.Quay(x2[20].X, x2[20].Y, hsx, hsy, gCD);
                            x2[21] = chd.Quay(x2[21].X, x2[21].Y, hsx, hsy, gCD);
                            ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                               x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo2(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                                     x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21]);                           
                        }
                        else if(phepbiendoi == 3)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text), hsy = Convert.ToInt32(y1cd.Text),
                              hsx1 = Convert.ToInt32(x2cd.Text), hsy1 = Convert.ToInt32(y2cd.Text);
                            hsy = -hsy; hsy1 = -hsy1;

                            if (hsx == 0 && hsy == 0) //Vẽ tâm của phép đối xứng qua điểm
                            {
                                DuongTron dtr = new DuongTron();
                                dtr.MidpointDuongTron(hsx1 * 10 + 500, hsy1 * 10 + 350, 2, bmp, Color.Red);
                                e.Graphics.DrawImage(bmp, 0, 0);
                            }

                            int A = hsy1 - hsy, B = hsx - hsx1, C = hsx1 * hsy - hsx * hsy1;
                            if (hsx != 0 || hsy != 0)   //Vẽ trục đối xứng 
                            {
                                DuongThang dt = new DuongThang();
                                PhepBienDoi pbd = new PhepBienDoi();
                                int y1, y2;
                                if (B == 0 && A != 0)
                                {
                                    dt.MidpointLine(hsx * 10 + 500, 1, hsx * 10 + 500, 658, bmp, Color.Red);
                                }
                                else if (A == 0 && B != 0)
                                {
                                    dt.MidpointLine(1, hsy * 10 + 350, 1018, hsy * 10 + 350, bmp, Color.Red);
                                }
                                else if (A == 0 && B == 0)
                                {
                                    dt.MidpointLine(500 - 350, 350 - 350, 500 + 350, 350 + 350, bmp, Color.Red);
                                }
                                else
                                {
                                    y1 = (-A * 30 - C) / B;
                                    y2 = (-A * 30 - C) / B;
                                    dt.MidpointLine(-30 * 10 + hsx * 10 + 500, -y1 * 10 + hsy * 10 + 350,
                                        30 * 10 + hsx1 * 10 + 500, y2 * 10 + hsy1 * 10 + 350, bmp, Color.Red);
                                }
                                e.Graphics.DrawImage(bmp, 0, 0);
                            }
                            //ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                            //    x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2,
                            //    bke1, bke2, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            x2[1] = chd.DoiXung(x2[1].X, x2[1].Y, hsx, hsy, hsx1, hsy1); x2[2] = chd.DoiXung(x2[2].X, x2[2].Y, hsx, hsy, hsx1, hsy1);
                            x2[3] = chd.DoiXung(x2[3].X, x2[3].Y, hsx, hsy, hsx1, hsy1); x2[4] = chd.DoiXung(x2[4].X, x2[4].Y, hsx, hsy, hsx1, hsy1);
                            x2[5] = chd.DoiXung(x2[5].X, x2[5].Y, hsx, hsy, hsx1, hsy1); x2[6] = chd.DoiXung(x2[6].X, x2[6].Y, hsx, hsy, hsx1, hsy1);
                            x2[7] = chd.DoiXung(x2[7].X, x2[7].Y, hsx, hsy, hsx1, hsy1); x2[8] = chd.DoiXung(x2[8].X, x2[8].Y, hsx, hsy, hsx1, hsy1);
                            x2[9] = chd.DoiXung(x2[9].X, x2[9].Y, hsx, hsy, hsx1, hsy1); x2[10] = chd.DoiXung(x2[10].X, x2[10].Y, hsx, hsy, hsx1, hsy1);
                            x2[11] = chd.DoiXung(x2[11].X, x2[11].Y, hsx, hsy, hsx1, hsy1); x2[12] = chd.DoiXung(x2[12].X, x2[12].Y, hsx, hsy, hsx1, hsy1);
                            x2[13] = chd.DoiXung(x2[13].X, x2[13].Y, hsx, hsy, hsx1, hsy1); x2[14] = chd.DoiXung(x2[14].X, x2[14].Y, hsx, hsy, hsx1, hsy1);
                            x2[15] = chd.DoiXung(x2[15].X, x2[15].Y, hsx, hsy, hsx1, hsy1); x2[16] = chd.DoiXung(x2[16].X, x2[16].Y, hsx, hsy, hsx1, hsy1);
                            x2[17] = chd.DoiXung(x2[17].X, x2[17].Y, hsx, hsy, hsx1, hsy1); x2[18] = chd.DoiXung(x2[18].X, x2[18].Y, hsx, hsy, hsx1, hsy1);
                            x2[19] = chd.DoiXung(x2[19].X, x2[19].Y, hsx, hsy, hsx1, hsy1); x2[20] = chd.DoiXung(x2[20].X, x2[20].Y, hsx, hsy, hsx1, hsy1);
                            x2[21] = chd.DoiXung(x2[21].X, x2[21].Y, hsx, hsy, hsx1, hsy1);
                            ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                               x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo2(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                                     x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21]);
                        }
                        else if (phepbiendoi == 4)
                        {
                            int hsx = Convert.ToInt32(x1cd.Text), hsy = Convert.ToInt32(y1cd.Text),
                               hsx1 = x2[15].X, hsy1 = 350;
                            //ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                            //    x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2,
                            //    bke1, bke2, bmp, Color.White);
                            //e.Graphics.DrawImage(bmp, 0, 0);
                            if(hsx>=0)
                            {
                                bkl1 *= hsx; bkn1 *= hsx; bke1 *= hsx;
                            }
                            else
                            {
                                bkl1 /= (-hsx); bkn1 /= (-hsx); bke1 /= (-hsx);
                            }
                            if(hsy>=0)
                            {
                                bkl2 *= hsy; bkn2 *= hsy; bke2 *= hsy;
                            }
                            else
                            {
                                bkl2 /= (-hsy); bkn2 /= (-hsy); bke2 /= (-hsy);
                            }
                            hsy = -hsy;
                            x2[1] = chd.TyLe(x2[1].X, x2[1].Y, hsx, hsy, hsx1, hsy1); x2[2] = chd.TyLe(x2[2].X, x2[2].Y, hsx, hsy, hsx1, hsy1);
                            x2[3] = chd.TyLe(x2[3].X, x2[3].Y, hsx, hsy, hsx1, hsy1); x2[4] = chd.TyLe(x2[4].X, x2[4].Y, hsx, hsy, hsx1, hsy1);
                            x2[5] = chd.TyLe(x2[5].X, x2[5].Y, hsx, hsy, hsx1, hsy1); x2[6] = chd.TyLe(x2[6].X, x2[6].Y, hsx, hsy, hsx1, hsy1);
                            x2[7] = chd.TyLe(x2[7].X, x2[7].Y, hsx, hsy, hsx1, hsy1); x2[8] = chd.TyLe(x2[8].X, x2[8].Y, hsx, hsy, hsx1, hsy1);
                            x2[9] = chd.TyLe(x2[9].X, x2[9].Y, hsx, hsy, hsx1, hsy1); x2[10] = chd.TyLe(x2[10].X, x2[10].Y, hsx, hsy, hsx1, hsy1);
                            x2[11] = chd.TyLe(x2[11].X, x2[11].Y, hsx, hsy, hsx1, hsy1); x2[12] = chd.TyLe(x2[12].X, x2[12].Y, hsx, hsy, hsx1, hsy1);
                            x2[13] = chd.TyLe(x2[13].X, x2[13].Y, hsx, hsy, hsx1, hsy1); x2[14] = chd.TyLe(x2[14].X, x2[14].Y, hsx, hsy, hsx1, hsy1);
                            x2[15] = chd.TyLe(x2[15].X, x2[15].Y, hsx, hsy, hsx1, hsy1); x2[16] = chd.TyLe(x2[16].X, x2[16].Y, hsx, hsy, hsx1, hsy1);
                            x2[17] = chd.TyLe(x2[17].X, x2[17].Y, hsx, hsy, hsx1, hsy1); x2[18] = chd.TyLe(x2[18].X, x2[18].Y, hsx, hsy, hsx1, hsy1);
                            x2[19] = chd.TyLe(x2[19].X, x2[19].Y, hsx, hsy, hsx1, hsy1); x2[20] = chd.TyLe(x2[20].X, x2[20].Y, hsx, hsy, hsx1, hsy1);
                            x2[21] = chd.TyLe(x2[21].X, x2[21].Y, hsx, hsy, hsx1, hsy1);

                            ve2.VeHinhXe(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                               x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            HienThiToaDo2(x2[1], x2[2], x2[3], x2[4], x2[5], x2[6], x2[7], x2[8], x2[9], x2[10], x2[11], x2[12],
                                     x2[13], x2[14], x2[15], x2[16], x2[17], x2[18], x2[19], x2[20], x2[21]);
                            if(bkl1 == bkl2)
                            {
                                labelh1.Text = "Hình tròn tâm L, bán kính r = " + (float)bkl1/10 ;
                                labelh2.Text = "Hình tròn tâm L, bán kính r = " + (float)bkn1/10;
                                labelh3.Text = "Hình tròn tâm G, bán kính r = " + (float)bkl1/10;
                                labelh4.Text = "Hình tròn tâm G, bán kính r = " + (float)bkn1/10;
                                labelh5.Text = "Hình tròn tâm A, bán kính r = " + (float)bke1/10;
                            }
                            else
                            {
                                labelh1.Text = "Elip tâm L, bán kính R = " + (float)bkl1 / 10 + "bán kính r = " + (float)bkl2 / 10;
                                labelh2.Text = "Elip tâm L, bán kính R = " + (float)bkn1 / 10 + "bán kính r = " + (float)bkn2 / 10;
                                labelh3.Text = "Elip tâm G, bán kính R = " + (float)bkl1 / 10 + "bán kính r = " + (float)bkl2 / 10;
                                labelh4.Text = "Elip tâm G, bán kính R = " + (float)bkn1 / 10 + "bán kính r = " + (float)bkn2 / 10;
                                labelh5.Text = "Elip tâm A, bán kính R = " + (float)bke1 / 10 + "bán kính r = " + (float)bke2 / 10;
                            }
                        }
                    }

                    if (chDong2 == true)
                    {
                        XoaTruc2D(e);
                        dth.MidpointLine(40, 351, 960, 351, bmp, Color.Chocolate); //Đường xe chạy
                        bonusx = 0;
                        ve2.VeHinhXe(x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14],
                                x[15], x[16], x[17], x[18], x[19], x[20], x[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.White);
                        x[1].X = 75 + bonusx; x[1].Y = 270; x[2].X = 75 + bonusx; x[2].Y = 280; x[3].X = 140 + bonusx; x[3].Y = 270;
                        x[4].X = 140 + bonusx; x[4].Y = 250; x[5].X = 130 + bonusx; x[5].Y = 240; x[6].X = 150 + bonusx; x[6].Y = 260;
                        x[7].X = 150 + bonusx; x[7].Y = 320; x[8].X = 150 + bonusx; x[8].Y = 295; x[9].X = 175 + bonusx; x[9].Y = 320;
                        x[10].X = 150 + bonusx; x[10].Y = 345; x[11].X = 125 + bonusx; x[11].Y = 320; x[12].X = 50 + bonusx; x[12].Y = 320;
                        x[13].X = 50 + bonusx; x[13].Y = 345; x[14].X = 50 + bonusx; x[14].Y = 295; x[15].X = 25 + bonusx; x[15].Y = 320;
                        x[16].X = 75 + bonusx; x[16].Y = 320; x[17].X = 100 + bonusx; x[17].Y = 310; x[18].X = 100 + bonusx; x[18].Y = 300;
                        x[19].X = 100 + bonusx; x[19].Y = 320; x[20].X = 143 + bonusx; x[20].Y = 280; x[21].X = 30 + bonusx; x[21].Y = 277;
                        int gq;
                        for(int i= 0; i<150;i++)
                        {
                            gq = 30;
                            for (int j = 1; j < 22; j++) x1[j] = x[j];
                            x[8] = chd.Quay(x[8].X, x[8].Y, x[7].X-500, x[7].Y-350, gq);
                            x[9] = chd.Quay(x[9].X, x[9].Y, x[7].X - 500, x[7].Y - 350, gq);
                            x[10] = chd.Quay(x[10].X, x[10].Y, x[7].X - 500, x[7].Y - 350, gq);
                            x[11] = chd.Quay(x[11].X, x[11].Y, x[7].X - 500, x[7].Y - 350, gq);

                            x[13] = chd.Quay(x[13].X, x[13].Y, x[12].X - 500, x[12].Y - 350, gq);
                            x[14] = chd.Quay(x[14].X, x[14].Y, x[12].X - 500, x[12].Y - 350, gq);
                            x[15] = chd.Quay(x[15].X, x[15].Y, x[12].X - 500, x[12].Y - 350, gq);
                            x[16] = chd.Quay(x[16].X, x[16].Y, x[12].X - 500, x[12].Y - 350, gq);

                            x[18] = chd.Quay(x[18].X, x[18].Y, x[17].X - 500, x[17].Y - 350, gq);
                            x[19] = chd.Quay(x[19].X, x[19].Y, x[17].X - 500, x[17].Y - 350, gq);

                            ve2.VeHinhXe(x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14],
                                x[15], x[16], x[17], x[18], x[19], x[20], x[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            ve2.VeHinhXe(x1[1], x1[2], x1[3], x1[4], x1[5], x1[6], x1[7], x1[8], x1[9], x1[10], x1[11], x1[12],
                                x1[13], x1[14], x1[15], x1[16], x1[17], x1[18], x1[19], x1[20], x1[21], bkl1, bkl2, bkn1, bkn2,
                                bke1, bke2, bmp, Color.White);
                            ve2.VeHinhXe(x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14],
                                x[15], x[16], x[17], x[18], x[19], x[20], x[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                            int kc;
                            for (int j = 1; j < 22; j++) x1[j] = x[j];
                            kc = 5;
                            x[1] = chd.TinhTien(x[1].X, x[1].Y, kc, 0); x[2] = chd.TinhTien(x[2].X, x[2].Y, kc, 0);
                            x[3] = chd.TinhTien(x[3].X, x[3].Y, kc, 0); x[4] = chd.TinhTien(x[4].X, x[4].Y, kc, 0);
                            x[5] = chd.TinhTien(x[5].X, x[5].Y, kc, 0); x[6] = chd.TinhTien(x[6].X, x[6].Y, kc, 0);
                            x[7] = chd.TinhTien(x[7].X, x[7].Y, kc, 0); x[8] = chd.TinhTien(x[8].X, x[8].Y, kc, 0);
                            x[9] = chd.TinhTien(x[9].X, x[9].Y, kc, 0); x[10] = chd.TinhTien(x[10].X, x[10].Y, kc, 0);
                            x[11] = chd.TinhTien(x[11].X, x[11].Y, kc, 0); x[12] = chd.TinhTien(x[12].X, x[12].Y, kc, 0);
                            x[13] = chd.TinhTien(x[13].X, x[13].Y, kc, 0); x[14] = chd.TinhTien(x[14].X, x[14].Y, kc, 0);
                            x[15] = chd.TinhTien(x[15].X, x[15].Y, kc, 0); x[16] = chd.TinhTien(x[16].X, x[16].Y, kc, 0);
                            x[17] = chd.TinhTien(x[17].X, x[17].Y, kc, 0); x[18] = chd.TinhTien(x[18].X, x[18].Y, kc, 0);
                            x[19] = chd.TinhTien(x[19].X, x[19].Y, kc, 0); x[20] = chd.TinhTien(x[20].X, x[20].Y, kc, 0);
                            x[21] = chd.TinhTien(x[21].X, x[21].Y, kc, 0);

                            ve2.VeHinhXe(x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14],
                                x[15], x[16], x[17], x[18], x[19], x[20], x[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            ve2.VeHinhXe(x1[1], x1[2], x1[3], x1[4], x1[5], x1[6], x1[7], x1[8], x1[9], x1[10], x1[11], x1[12], x1[13], x1[14],
                                x1[15], x1[16], x1[17], x1[18], x1[19], x1[20], x1[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.White);
                            ve2.VeHinhXe(x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14],
                                x[15], x[16], x[17], x[18], x[19], x[20], x[21], bkl1, bkl2, bkn1, bkn2, bke1, bke2, bmp, Color.Red);
                            e.Graphics.DrawImage(bmp, 0, 0);
                        }
                    }

                }
            }
            else if(trucToaDo2D == 2)
            {
                Truc3D truc3D = new Truc3D();
                truc3D.VeTruc(bmp,e);

                if (veHHCN == true)
                {
                    HinhHopChuNhat hinhHop = new HinhHopChuNhat();
                    double a, b, c, x, y, z;
                    a = Convert.ToDouble(hcn2.Text);
                    b = Convert.ToDouble(hcn3.Text);
                    c = Convert.ToDouble(hcn4.Text);
                    x = Convert.ToDouble(hcn6.Text);
                    y = Convert.ToDouble(hcn7.Text);
                    z = Convert.ToDouble(hcn8.Text);
                    double a1 = a, b1 = b, c1 = c, x1 = x, y1 = y, z1 = z;
                    c1 *= 10; c1 /= 2; z1 *= 10; z1 /= 2; a1 *= 10; b1 *= 10; x1 *= 10; y1 *= 10;
                    hinhHop.VeHinh(a, b, c, x, y, z, e, bmp);
                    diemA.Location = new Point((int)(a1 - c1 + 500), (int)(351 - b1 + c1));
                    diemB.Location = new Point((int)(a1 - c1 + x1 + 500), (int)(351 - b1 + c1));
                    diemC.Location = new Point((int)(a1 - c1 + 500), (int)(340 - b1 + c1 - y1));
                    diemD.Location = new Point((int)(a1 - c1 - z1 + 500), (int)(351 - b1 + c1 + z1));
                    diemE.Location = new Point((int)(a1 - c1 + x1 + 501), (int)(350 - b1 + c1 - y1));
                    diemF.Location = new Point((int)(a1 - c1 - z1 + x1 + 500), (int)(351 - b1 + c1 + z1));
                    diemG.Location = new Point((int)(a1 - c1 - z1 + 489), (int)(350 - b1 + c1 + z1 - y1));
                    diemH.Location = new Point((int)(a1 - c1 - z1 + x1 + 501), (int)(350 - b1 + c1 + z1 - y1));


                    d1.Text = "A(" + a + "," + b + "," + c + ")";
                    d2.Text = "B(" + (a + x) + "," + b + "," + c + ")";
                    d3.Text = "C(" + a + "," + (b + y) + "," + c + ")";
                    d4.Text = "D(" + a + "," + b + "," + (c + z) + ")";
                    d5.Text = "E(" + (a + x) + "," + (b + y) + "," + c + ")";
                    d6.Text = "F(" + (a + x) + "," + b + "," + (c + z) + ")";
                    d7.Text = "G(" + a + "," + (b + y) + "," + (c + z) + ")";
                    d8.Text = "H(" + (a + x) + "," + (b + y) + "," + (c + z) + ")";
                    labelh1.Text = "Hình chữ nhật ABEC";
                    labelh2.Text = "Hình chữ nhật DFHG";
                    labelh3.Text = "Hình bình hành ABFD";
                    labelh4.Text = "Hình bình hành ACGD";
                    labelh5.Text = "Hình bình hành CEHG";
                    labelh6.Text = "Hình bình hành BEHF";

                    labelh1.Visible = labelh2.Visible = labelh3.Visible = labelh4.Visible = labelh5.Visible = labelh6.Visible =
                    diemA.Visible = diemB.Visible = diemC.Visible = diemD.Visible = diemE.Visible = diemF.Visible = diemG.Visible
                = diemH.Visible = d1.Visible = d2.Visible = d3.Visible = d4.Visible = d5.Visible = d6.Visible = d7.Visible = d8.Visible = true;

                }

                if(veHTT == true)
                {

                    HinhTruTron hinhTru = new HinhTruTron();
                    double x, y, z, r, c;
                    x = Convert.ToDouble(hcn2.Text);
                    y = Convert.ToDouble(hcn3.Text);
                    z = Convert.ToDouble(hcn4.Text);
                    r = Convert.ToDouble(hcn6.Text);
                    c = Convert.ToDouble(hcn8.Text);
                    double x1 = x, y1 = y, z1 = z, r1 = r, c1 = c;
                    x1 *= 10;  y1 *= 10; z1 /= 2; z1 *= 10; r1 *= 10; c1 *= 10;
                    hinhTru.VeHinh(x, y, z, r, c, e, bmp);
                    diemA.Location = new Point((int)(x1 - z1 + 501), (int)(350 - y1 + z1));
                    diemB.Location = new Point((int)(x1 - z1 + r1 + 501), (int)(350 - y1 + z1));
                    diemC.Location = new Point((int)(x1 - z1 + r1 + 501), (int)(350 - y1 + z1 - c1));
                    diemD.Location = new Point((int)(x1 - z1 - r1 + 489), (int)(350 - y1 + z1));
                    diemE.Location = new Point((int)(x1 - z1 - r1 + 489), (int)(350 - y1 + z1 - c1));
                    diemF.Location = new Point((int)(x1 - z1 + 501), (int)(350 - y1 + z1 - c1));

                    d1.Text = "A(" + x + "," + y + "," + z + ")";
                    d2.Text = "B(" + (r + x) + "," + y + "," + z + ")";
                    d3.Text = "C(" + x + "," + (c + y) + "," + z + ")";
                    d4.Text = "D(" + (x - r) + "," + y + "," + z + ")";
                    d5.Text = "E(" + (x - r) + "," + (y + c) + "," + z + ")";
                    d6.Text = "F(" + x + "," + (y + c) + "," + z + ")";
                    labelh1.Text = "Hình Elip tâm A, bán kính R = " + r + ", r = " + r / 2;
                    labelh2.Text = "Hình Elip tâm F, bán kính R = " + r + ", r = " + r / 2;
                    labelh3.Text = "";
                    labelh4.Text = "";
                    labelh5.Text = "";
                    labelh6.Text = "";
                    labelh1.Visible = labelh2.Visible = labelh3.Visible = labelh4.Visible = labelh5.Visible = labelh6.Visible =
                    diemA.Visible = diemB.Visible = diemC.Visible = diemD.Visible = diemE.Visible = diemF.Visible 
                        = d1.Visible = d2.Visible = d3.Visible = d4.Visible = d5.Visible = d6.Visible = true;

                }
            }
            bmp.Dispose();
        }
    }
}
