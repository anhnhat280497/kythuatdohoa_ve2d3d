﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTDH
{
    class ELip
    {
        void Ve4diem(int xc, int yc, int x, int y, Bitmap bmp, Color cl, bool nd)//ve 4 diem doi xung. xc, yc:tâm. x,y: x,y1 ở hàm dưới
        {
            if (xc + x >= 0 && xc + x < 1018 && yc + y >= 0 && yc + y < 685) bmp.SetPixel(xc + x, yc + y, cl); //phần 4
            if (xc - x >= 0 && xc - x < 1018 && yc + y >= 0 && yc + y < 685) bmp.SetPixel(xc - x, yc + y, cl); // phần 3
            
            if (xc - x >= 0 && xc - x < 1018 && yc - y >= 0 && yc - y < 685) bmp.SetPixel(xc - x, yc - y, cl); //phần 2
            if (xc + x >= 0 && xc + x < 1018 && yc - y >= 0 && yc - y < 685) bmp.SetPixel(xc + x, yc - y, cl); //phần 1
            if (nd == true && (x % 6 == 0 || (x - 1) % 6 == 0 || (x + 1) % 6 == 0))
            {
                if (xc - x >= 0 && xc - x < 1018 && yc - y >= 0 && yc - y < 685) bmp.SetPixel(xc - x, yc - y, Color.White); //phần 2
                if (xc + x >= 0 && xc + x < 1018 && yc - y >= 0 && yc - y < 685) bmp.SetPixel(xc + x, yc - y, Color.White); //phần 1
            }
        }
        public void HinhELip(int x, int y, int a, int b, Bitmap bmp, Color cl) // tâm x,y : rộng a, cao b;
        {
            float a2, b2;
            double p;
            int x1 = 0, y1 = b;
            a2 = a*a;
            b2 = b*b;

            if (x >= 0 && x < 1018 && y >= 0 && y < 685) bmp.SetPixel(x, y, cl);
            Ve4diem(x, y, x1, y1, bmp, cl,false);

            //ve nhanh thu 1(tu tren xuong )
            while (((float)b2 / a2) * x1 <= y1)
            {
                p = b2*(x1+1)*(x1+1) + a2*(y1-0.5)*(y1-0.5) - a2 * b2;
                if (p >= 0)
                {
                    y1--;
                }
                x1++;
                Ve4diem(x, y, x1, y1, bmp, cl,false);
            }
            //ve nhanh thu 2(tu duoi len )
            y1 = 0;
            x1 = a;

            Ve4diem(x, y, x1, y1, bmp, cl,false);
           // p = 2 * ((float)a2 / b2) - 2 * a + 1;
            while (((float)a2 / b2) * y1 <= x1)
            {
                p = b2 * (x1 - 0.5) * (x1 - 0.5) + a2*(y1 + 1) * (y1 + 1) - a2 *b2;
                if (p >= 0)
                {
                    x1 --;
                }
                y1 ++;
                Ve4diem(x, y, x1, y1, bmp, cl,false);
            }
        }
        public void NetDut(int x, int y, int a, int b, Bitmap bmp, Color cl) // tâm x,y : rộng a, cao b;
        {
            float a2, b2;
            double p;
            int x1 = 0, y1 = b;
            a2 = a * a;
            b2 = b * b;

            if (x >= 0 && x < 1018 && y >= 0 && y < 685) bmp.SetPixel(x, y, cl);
            Ve4diem(x, y, x1, y1, bmp, cl,true);

            //ve nhanh thu 1(tu tren xuong )
            while (((float)b2 / a2) * x1 <= y1)
            {
                p = b2 * (x1 + 1) * (x1 + 1) + a2 * (y1 - 0.5) * (y1 - 0.5) - a2 * b2;
                if (p >= 0)
                {
                    y1--;
                }
                x1++;
                Ve4diem(x, y, x1, y1, bmp, cl,true);
            }
            //ve nhanh thu 2(tu duoi len )
            y1 = 0;
            x1 = a;

            Ve4diem(x, y, x1, y1, bmp, cl,true);
            p = 2 * ((float)a2 / b2) - 2 * a + 1;
            while (((float)a2 / b2) * y1 <= x1)
            {
                p = b2 * (x1 - 0.5) * (x1 - 0.5) + a2 * (y1 + 1) * (y1 + 1) - a2 * b2;
                if (p >= 0)
                {
                    x1--;
                }
                y1++;
                Ve4diem(x, y, x1, y1, bmp, cl,true);
            }
        }
    }
}
