﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KTDH
{
    class HinhHopChuNhat
    {
        public void VeHinh(double a, double b, double c, double x, double y, double z, PaintEventArgs e, Bitmap bmp)
        { //Tọa độ điểm gốc(Chọn điểm A của hình) x, y, z: Các chiều.
            DuongThang dth = new DuongThang();
            c *= 10; c/= 2; z *= 10; z /= 2; a *= 10; b *= 10; x *= 10; y *= 10;
            Point d0 = new Point((int)(a - c + 500), (int)(350 - b + c));
            Point d1 = new Point((int)(a - c + x + 500), (int)(350 - b + c));
            Point d2 = new Point((int)(a - c + 500), (int)(350 - b + c - y));
            Point d3 = new Point((int)(a - c - z + 500), (int)(350 - b + c + z));
            Point d4 = new Point((int)(a - c + x + 500), (int)(350 - b + c - y));
            Point d5 = new Point((int)(a - c - z + x + 500), (int)(350 - b + c + z));
            Point d6 = new Point((int)(a - c - z + 500), (int)(350 - b + c + z - y));
            Point d7 = new Point((int)(a - c - z + x + 500), (int)(350 - b + c + z - y));

            dth.NetDut(d0.X, d0.Y,d1.X, d1.Y, bmp, Color.Red); //0 ->1
            dth.NetDut(d0.X, d0.Y, d2.X, d2.Y, bmp, Color.Red); //0->2
            dth.NetDut(d0.X, d0.Y, d3.X, d3.Y, bmp, Color.Red); //0->3
            dth.MidpointLine(d5.X, d5.Y, d1.X, d1.Y, bmp, Color.Red); //5 ->1
            dth.MidpointLine(d4.X, d4.Y, d1.X, d1.Y, bmp, Color.Red); //4 ->1
            dth.MidpointLine(d4.X, d4.Y, d2.X, d2.Y, bmp, Color.Red); //4 ->2
            dth.MidpointLine(d6.X, d6.Y, d2.X, d2.Y, bmp, Color.Red); //6 ->2
            dth.MidpointLine(d6.X, d6.Y, d3.X, d3.Y, bmp, Color.Red); //6->3
            dth.MidpointLine(d5.X, d5.Y, d3.X, d3.Y, bmp, Color.Red); //5->3
            dth.MidpointLine(d5.X, d5.Y, d7.X, d7.Y, bmp, Color.Red); //5->7
            dth.MidpointLine(d6.X, d6.Y, d7.X, d7.Y, bmp, Color.Red); //6->7
            dth.MidpointLine(d4.X, d4.Y, d7.X, d7.Y, bmp, Color.Red); //4->7
            e.Graphics.DrawImage(bmp, 0, 0);
        }
    }
}
