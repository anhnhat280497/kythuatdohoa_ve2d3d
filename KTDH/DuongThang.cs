﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTDH
{
    class DuongThang
    {
        void HoanVi(ref int a, ref int b)
        {
            int t;
            t = a;
            a = b;
            b = t;
        }
        void MidpointLineXY(int x1, int y1, int x2, int y2, Bitmap bmp, Color a, bool nd) //x++ , y++ Dx>=Dy 
        {
            if (x1 > x2)
            {
                HoanVi(ref x1, ref x2);
                HoanVi(ref y1, ref y2);
            }
            int x = x1, y = y1, Dy = y2-y1;
            if (x1 < 1018 && y1 < 685 && x1 >= 0 && y1 >= 0) bmp.SetPixel(x1, y1, a);
            double p;
            float A = y2 - y1, B = -(x2 - x1), C = x2 * y1 - x1 * y2;
            while (x < x2)
            {
                if (y1 < y2 && Dy != 0)
                {
                    p = A * (x + 1) + B * (y + 0.5) + C;
                    if (p >= 0) y++;
                }
                else if (y1 > y2 && Dy != 0)
                {
                    p = A * (x + 1) + B * (y - 0.5) + C;
                    if (p <= 0) y--;
                }
                x++;
                if(x < 1018 && y < 685 && x >= 0 && y >= 0)
                {
                    if (nd == true && (x % 6 == 0 || (x + 1) % 6 == 0 || (x - 1) % 6 == 0)) bmp.SetPixel(x, y, Color.White);
                    else bmp.SetPixel(x, y, a);
                }
            }
        }
        void MidpointLineYX(int x1, int y1, int x2, int y2, Bitmap bmp, Color a, bool nd) //y++ , x++ Dy>Dx 
        {
            if (y1 > y2)
            {
                HoanVi(ref x1, ref x2);
                HoanVi(ref y1, ref y2);
            }
            int x = x1, y = y1, Dx = x2-x1;
            if (x1 < 1018 && y1 < 685 && x1 >= 0 && y1 >= 0) bmp.SetPixel(x1, y1, a);
            double p;
            float A = y2 - y1, B = -(x2 - x1), C = x2 * y1 - x1 * y2;
            while (y < y2)
            {
                if (x1 < x2 && Dx != 0)
                {
                    p = A * (x + 0.5) + B * (y + 1) + C;
                    if (p <= 0) x++;
                }
                else if (x1 > x2 && Dx != 0)
                {
                    p = A * (x - 0.5) + B * (y + 1) + C;
                    if (p >= 0) x--;
                }
                y++;
                if (x < 1018 && y < 685 && x >= 0 && y >= 0)
                {
                    if (nd == true &&  (y % 6 == 0 || (y+1)%6==0 || (y-1)%6==0)) bmp.SetPixel(x, y, Color.White);
                    else bmp.SetPixel(x, y, a);
                }
            }
        }
        public void MidpointLine(int x1, int y1, int x2, int y2, Bitmap bmp, Color a)
        {
            int Dx = Math.Abs(x2 - x1), Dy = Math.Abs(y2 - y1);

            if (Dx >= Dy) MidpointLineXY(x1, y1, x2, y2, bmp, a, false);
            else if (Dx < Dy) MidpointLineYX(x1, y1, x2, y2, bmp, a, false);
        }
        public void NetDut(int x1, int y1, int x2, int y2, Bitmap bmp, Color a)
        {
            int Dx = Math.Abs(x2 - x1), Dy = Math.Abs(y2 - y1);

            if (Dx >= Dy) MidpointLineXY(x1, y1, x2, y2, bmp, a, true);
            else if (Dx < Dy) MidpointLineYX(x1, y1, x2, y2, bmp, a, true);
        }
    }
}
