﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTDH
{
    class Ve2Hinh2D
    {
        public void VeHinhNguoi(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, 
            int x5, int y5, int x6, int y6, int x7, int y7, int R, int r, Bitmap bmp, Color a)
        {
            DuongTron dtr = new DuongTron();
            DuongThang dt = new DuongThang();
            ELip el = new ELip();
            if(R == r)
            {
                dtr.MidpointDuongTron(x1, y1, Math.Abs(R), bmp, a); // đầu
            }
            else
            {
                el.HinhELip(x1, y1, Math.Abs(R), Math.Abs(r), bmp, a);
            }
            dt.MidpointLine(x2, y2, x3, y3, bmp, a); //mình

            dt.MidpointLine(x2, y2, x4, y4, bmp, a); // tay trái
            dt.MidpointLine(x2, y2, x5, y5, bmp, a); // tay phải

            dt.MidpointLine(x6, y6, x3, y3, bmp, a); // chân trái
            dt.MidpointLine(x3, y3, x7, y7, bmp, a); // chân phải

        }
        public void VeHinhKiem(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5, 
            int x6, int y6, int x7, int y7, int x8, int y8, int x9, int y9, int x10, int y10, int x11, int y11,
            int x12, int y12, int x13, int y13, Bitmap bmp, Color a)
        {
            DuongThang dt = new DuongThang();
            dt.MidpointLine(x1, y1, x2, y2, bmp, a);
            dt.MidpointLine(x2, y2, x3, y3, bmp, a);
            dt.MidpointLine(x4, y4, x1, y1, bmp, a);//đuôi

            dt.MidpointLine(x5, y5, x6, y6, bmp, a);
            dt.MidpointLine(x6, y6, x9, y9, bmp, a);
            dt.MidpointLine(x9, y9, x10, y10, bmp, a);
            dt.MidpointLine(x10, y10, x5, y5, bmp, a);

            dt.MidpointLine(x7, y7, x11, y11, bmp, a);
            dt.MidpointLine(x8, y8, x12, y12, bmp, a);
            dt.MidpointLine(x11, y11, x13, y13, bmp, a);
            dt.MidpointLine(x12, y12, x13, y13, bmp, a);
        }

        public void VeHinhXe(Point a1, Point b2, Point c3, Point d4, Point e5, Point f6, Point g7, Point h8, Point i9,
            Point j10, Point k11, Point l12, Point m13, Point n14, Point o15, Point p16, Point q17, Point r18,
            Point s19, Point t20, Point u21, int bkl1, int bkl2, int bkn1, int bkn2, int bke1, int bke2, Bitmap bmp, Color cl)
        {
            DuongTron dtr = new DuongTron();
            DuongThang dt = new DuongThang();
            ELip el = new ELip();

            if(bkl1 == bkl2) // 2 bán kính bằng nhau thì vẽ hình tròn, còn k vẽ elip(phục vụ phép tỷ lệ)
            {
                dtr.MidpointDuongTron(l12.X, l12.Y, Math.Abs(bkl1), bmp, cl); //30
                dtr.MidpointDuongTron(g7.X, g7.Y, bkl1, bmp, cl);
                dtr.MidpointDuongTron(l12.X, l12.Y, bkn2, bmp, cl); //25
                dtr.MidpointDuongTron(g7.X, g7.Y, bkn2, bmp, cl);
                dtr.MidpointDuongTron(a1.X, a1.Y, bke1, bmp, cl); //15,5
            }
            else
            {
                el.HinhELip(l12.X, l12.Y, bkl1, bkl2, bmp, cl); //30
                el.HinhELip(g7.X, g7.Y, bkl1, bkl2, bmp, cl);
                el.HinhELip(l12.X, l12.Y, bkn1, bkn2, bmp, cl); //25
                el.HinhELip(g7.X, g7.Y, bkn1, bkn2, bmp, cl);
                el.HinhELip(a1.X, a1.Y, bke1, bke2, bmp, cl); //15,5
            }
            dt.MidpointLine(a1.X, a1.Y, b2.X, b2.Y, bmp, cl);
            dt.MidpointLine(l12.X, l12.Y, b2.X, b2.Y, bmp, cl);
            dt.MidpointLine(b2.X, b2.Y, c3.X, c3.Y, bmp, cl);
            dt.MidpointLine(g7.X, g7.Y, c3.X, c3.Y, bmp, cl);
            dt.MidpointLine(d4.X, d4.Y, c3.X, c3.Y, bmp, cl);
            dt.MidpointLine(d4.X, d4.Y, e5.X, e5.Y, bmp, cl);
            dt.MidpointLine(f6.X, f6.Y, e5.X, e5.Y, bmp, cl);
            dt.MidpointLine(b2.X, b2.Y, q17.X, q17.Y, bmp, cl);
            dt.MidpointLine(l12.X, l12.Y, q17.X, q17.Y, bmp, cl);
            dt.MidpointLine(r18.X, r18.Y, q17.X, q17.Y, bmp, cl);
            dt.MidpointLine(s19.X, s19.Y, q17.X, q17.Y, bmp, cl);
            dt.MidpointLine(t20.X, t20.Y, q17.X, q17.Y, bmp, cl);
            dt.MidpointLine(u21.X, u21.Y, b2.X, b2.Y, bmp, cl);
            dt.MidpointLine(g7.X, g7.Y, h8.X, h8.Y, bmp, cl);
            dt.MidpointLine(g7.X, g7.Y, i9.X, i9.Y, bmp, cl);
            dt.MidpointLine(g7.X, g7.Y, j10.X, j10.Y, bmp, cl);
            dt.MidpointLine(g7.X, g7.Y, k11.X, k11.Y, bmp, cl);
            dt.MidpointLine(l12.X, l12.Y, m13.X, m13.Y, bmp, cl);
            dt.MidpointLine(l12.X, l12.Y, n14.X, n14.Y, bmp, cl);
            dt.MidpointLine(l12.X, l12.Y, o15.X, o15.Y, bmp, cl);
            dt.MidpointLine(l12.X, l12.Y, p16.X, p16.Y, bmp, cl);


        }
    }
}
